# poolCNVcomp

The role of copy-number variation in the reinforcement of sexual 
isolation between the two European subspecies of the house mouse. North et al. 
(2020) Philos. Trans. R. Soc. Lond., B, Biol. Sci.

### Background

This repository is an adaptation of the poolDiffCNV  repo. (https://github.com/andrewkern/poolDiffCNV).

We apply the Python scripts by Schrider et al. with some alterations, with the 
aim of clustering and analysing tandem duplications and deletions 
(collectively, Copy-Number Variants, CNVs) in a similar manner such that 
their frequencies are more directly comparable. 

We alter the Python scripts for Step 1 to read .bam files. We replace Step 2 
scripts with a new clustering algorithm. The distanceCutoff parameter is 
altered in Step 3 and Step 4 is altered to read .bam files. 
The pipeline is otherwise the same. We also include scripts used in downstream
analyses.

Any use of the Python scripts available here should cite Schrider, Begun & 
Hahn:

Schrider, DR, Hahn, MW, and Begun, DJ. (2016) Parallel Evolution of Copy-Number Variation Across Continents in Drosophila melanogaster. Molecular Biology and Evolution. 33: 1308-1316.

Schrider, DR, Begun, DJ, and Hahn MW (2013) Detecting highly differentiated copy number variants from pooled population sequencing. Pacific Symposium on Biocomputing. 18:344-355.


### Step 1: Identify distant and everted read-pairs

We first identify distant reads. For each pool and for each of 20 chromosomes 
(19 autosomes and X; insufficient coverage could be achieved for Y), we subset 
inserts that exceeded the parameter insertSizeCutoff, which represents the upper
first percentile of the insert size distribution for each respective pool. 
For Step 1d we apply Schrider’s findEvertedInserts.py
Similarly, Step1 for everted reads (Step 1e) was applied using the 
findEvertedInserts.py script from Schrider et al (2013), 
altered to read .bam files. The bash script step1.sh executes these iteratively
over chromosomes and samples (pools).

### Step 2: Cluster distant and everted read-pairs within pools to identify deletions and tandem duplications

Multiple discordant read-pairs caused by a CNV event will map to the same 
location, so nearby discordant reads need to be clustered to distinguish 
distinct CNV events from singleton discordant read-pairs. 
Rather than using the .py script developed by Schrider et al. (2013), 
we implemented a similar but faster approach (see Suppl. Figure 3).

The output of step2e.sh is everted read-pair clusters (tandem duplications).
For chromosome X pool 2, for example, the output of step 1 assumes the following 
subdirectory structure: ~/step2/data/ChrX/cXp2e.txt
Similarly, the output of step2d.sh is distant read-pair clusters (deletions).

### Step 3: Match corresponding CNVs between pools and calcualte the nornalised number of supporting read-pairs in each pool

Step 3 matches homologous CNV coordinates between pools in a pairwise fashion, 
and also calculates the normalised number of supporting read pairs in each pool.

The first criterion to identify differentiated CNVs between two populations is 
the difference between the number of read-pairs supporting a mutation in each 
population, used as proxy for allele frequency difference. In order to compare 
CNV events across pools, clusters must be matched to corresponding CNV events 
across pools. Clusters are considered part of the same event if their 
coordinates are within a distance, distanceCutoff, from one another when two 
pools are compared. We set the distanceCutoff parameter to equal half the mean 
of the two CNVs being compared. 

Apart from redefining distanceCutoff, we use the script written by 
Schrider et al. (2013; ‘combineDistantClustersAccrossPools.py’). This step 
therefore reports whether each polymorphism present in one sample is present 
in the other, and also reports the number of read-pairs supporting the event 
in each population. The number of supporting read-pairs is normalised by the 
empirical read depth in each pool (extracted at step 1) to account for 
inter-pool differences in depth.

step3.sh calculates the normalisation constants and uses these as input when 
executing combineDistantClustersAccrossPoolsHN.py (for deletions) and 
combineEvertedClustersAcrossPoolsHN.py (for tandem duplications). 

The output of step 3 (as well as steps 4, 4.5 and 5) is still done one a 
per-chromosome basis but is organised into comparisons rather than 
chromosomes (as is the case for steps 1 and 2). For example, the results of 
comparison 1 (a contrast of pools 2 and 3) for distant read-
clusters (deletions) on chromosome 10 is stored under the following sub-
directory structure: s3/data/comparison1/c10P2vsP3_distant.tsv

### Step 4: Calculate relative read depth differences

The second criterion to identify deletions that differ in frequency between 
two populations is the read-depth difference between two populations. 
In Step 4, for each deletion event and duplication event we calculate the 
read depth for each of the populations being compared. The ratio of these 
depths is then calculated for a given population comparison. 
For this calculation we ignore masked regions of the genome that are 
highly repetitive. We apply a variation of countReadPairsInCNV.py altered 
such that it can read .bam files using PySam. 
The masked regions .bed file was accessed via: https://genome.ucsc.edu/cgi-bin/hgTables

The bash script step4.sh takes the output from step 3 and applies it to the 
script step4_countReadPairsInCNV-KH-pySam.py

### Step 4.5: Append additional allele frequency information to each duplication and deletion event

In order to know whether the read depth ratio is significantly different 
between pools, we compared observed read depth ratios to an empirical 
distribution. The empirical distribution was generated by measuring read depth 
ratios for many regions that belong to a specific size class. 
Each observed CNV event was assigned to a size class. Each size class has a 
known upper 95% threshold and lower 5% threshold for the expected difference in
read depth, based on empirical sampling of the data. CNVs that exceed the 
upper or lower thresholds are labelled. 

Confirmed deletions and duplications are those for which the read depth 
ratio is extreme in the same “direction” as the difference in the number 
of supporting inserts. For example, if there is a duplication in pool A, there 
should be more everted read pairs in A compared to B and a higher read depth in
A compared to B. CNVs that do not conform to this are labelled as 
“false positives.” The script step4pt5.sh implements this.

### Step 5: Label CNVs that are divergent between Choosy and Non-Choosy populations

In the 5th step (step5.sh) we simply subset the number of putatively
differentiated deletions and tandem duplications between two focal populations 
depending on the two criteria described above: extreme read depth ratios and 
extreme differences in the number of supporting inserts.

### Step 6: Subset CNVs that are highly differentiated across replicate comparisons.

Based on the values assigned in Steps 4 and 5, we subset the positions of 
deletions that are highly differentiated between Choosy and Non-Choosy 
populations and which have overlapping positions. The same principle is applied 
to the Control Test comparisons. For each test, a distinction is made between 
coordinates that overlap perfectly (“identical by state”; those which have 
identical start and end chromosomal coordinates), and those that overlap 
almost-perfectly (“non-identical by state”; those which overlap and differ by 
no more than 95% of their mean size).

More specifically, in step6.sh, the bedtools closest -d command is used to 
determine the extent of overlap between a given CNV in the first pool and the 
closest CNV in the second pool. The ratio of the two CNV sizes is then 
calcualted. If this value is between 0.95 and 1.05, the CNV is retained. If, 
additionally, the difference in both the start and end coordinates of the two 
comparisons is also zero, these CNV are marked as “identical by state.” 

Following step 6, "false positives" – i.e. those identified in both the Focal
Test and Control Test (see main text) – are removed. This step is simply a way 
of subsetting data (as is Step 5) and it could be completed – no doubt more 
efficiently – in a coding language such as R. Nonetheless, the comments in 
step6.sh explain exactly how this final step was achieved, and define the each 
of the 49 columns in the output data.

### Downstream analysis: test for a heterogeneous density of CNVs throughout the genome

We want to know whether CNVs have a clustered distributed throughout the genome. 
We use a simple, parameter-free statistical test. Two input files are required: 
(1) CNV coordinates and (2) a set of chromosome lengths. In our case, CNV 
coordinates can be extracted from the output of step 6 (see step6.sh for a 
description of the relevant fields). An example of one row of this tab-delimited
file is:

chr1	106744056	106747328

The first column is the chromosome, the second is the start coordinate and the 
third is the end coordinate of the CNV. 
The genome file is formatted as follows:

chr1	195471971

The first column is the chromosome name (using the same naming convention as
the CNV coordinate file), the second column is the chromosome length in bp.
This information should come from the reference genome used to identify CNV (in
our case, GRCm38).  

We use bedtools makewindows to split the genome into windows of constant size 
and record the observed number of CNVs per window. To generate a null 
expectation of CNVs/Mb assuming a homogenous density, we use bedtools 
shuffle to randomly redistribute CNVs throughout chromosomes. This is done 
10,000 times to generate a null distribution. CNV_density_test.sh
implements this. The p value of a test for a heterogeneous distribution 
is just the the number of permutations that exceed the observed density 
divided by the number of permutations. 

## First, split the genome into windows

bedtools makewindows -g mouse.genome -w 1000000 | sort -k 1,1 -k2,2n > 1Mbwindows.tsv

# Record the observed CNV density
bedmap --delim '\t' --echo --count 1Mbwindows.tsv <(bedops --everything CNV_coords.tsv) > Observed_CNV.txt
awk '{ total += $4 } END { print total/NR }' Observed_CNV.txt # This is CNV/Mb


# Define the permutation test:
function overlap() {
number=$1
RandNum=$2
CNVfile=$3
ExpectationDistribution=$4

bedtools shuffle -noOverlapping -i $CNVfile -g mouse.genome | sort -k 1,1 -k2,2n > permute${number}.tsv
bedmap --delim '\t' --echo --count 1Mbwindows.tsv <(bedops --everything permute${number}.tsv) > Expected${number}.txt
awk '{ total += $4 } END { print total/NR }' Expected${number}.txt >> $ExpectationDistribution
rm -rf Expected${number}.txt
rm -rf  permute${number}.tsv
}

# Implement 10,000 permutations
touch expectedDist_CNV.txt # expectedDist_CNV.txt will store the permutations
for number in in {1..10000}
do
    overlap $number $RANDOM CNV_coords.tsv expectedDist_CNV.txt
done

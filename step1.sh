# Define function for running the script to identify distant reads:
function step1d(){

    chr=$1
    pool=$2

    # Sequence data available on Shot Read Archive, bioproject PRJNA603262
    ficBam=filtered_Test_All_reads_GRCm38_Pool${pool}.sorted.reheaded.ddup.bam.REF_${chr}.bam


    samtools view $ficBam | awk '{ if ($5 >= 20 && $9 <= 2000) print $9 }' > ISIZE_distribution_c${chr}p${pool}.txt # Extract the insert size distribution

    sort -n ISIZE_distribution_c${chr}p${pool}.txt  | awk '{all[NR] = $0} END{print all[int(NR*0.99 - 0.01)]}' > ISIZE99_c${chr}p${pool}.txt # Compute the  the  99th percentile of the insert size distribution

    insertSizeCutoff=$(cat ISIZE99_c${chr}p${pool}.txt) # Define the variable insertSizeCutoff

    samtools view $ficBam | python ~/step1/step1_findDistantInserts.py $insertSizeCutoff  > c${chr}p${pool}d.txt

    rm -rf ISIZE_distribution_c${chr}p${pool}.txt # Remove the insert size distribution data

    rm -rf ISIZE99_c${chr}p${pool}.txt # Remove the 99th perentile data


}

# Define function for running the script to identify everted reads:

function step1e(){

    chr=$1
    pool=$2

    ficBam=filtered_Test_All_reads_GRCm38_Pool${pool}.sorted.reheaded.ddup.bam.REF_${chr}.bam

    samtools flagstat $ficBam | sed '9q;d'| cut -d ' ' -f 1 | cat > d${pool}_c${chr}.txt # Save read depth information for later calculation of normConst

    samtools view $ficBam | python ~/step1/step1_findEvertedInserts.py > c${chr}p${pool}e.txt # Identify everted reads

    rm -rf $ficBam # Remove the reference .bam file

}


for chr in `seq 1 19` X # iterate across all chromosomes
do
  cd ~/step1/data/Chr$chr # move to the relevant chromosome subdirectory

for pool in `seq 1 7` # iterate across all pools
  do
     step1d $chr $pool
     step1e $chr $pool
  done


  cd ..
done

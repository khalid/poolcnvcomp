function step2e() {

distFile=$1 #file from STEP1 everted test
MaxinsertSize=$2 #the maximum insert size
minNumSupportingInserts=$3  #specifies the minimum number of everted read pairs required to retain a cluster
resFile=$4 #result file in poolDiffCNV format
summary=$5 # result file with the distribution of clusters size

awk -v maxIsize=$MaxinsertSize -v resF=$resFile -v sumaryF=$summary -v numEvertInsertCutoff=$minNumSupportingInserts  '
BEGIN {nbRS=0; nbIS=0; OFS="\t"}
{

    if (NR==1)
    {
        cl = 1;
        ls = $3;
        min_ls = ls;
        le = $4;
        rs = $9;
        # In the first cluster, RS is the minimum RS
        min_rs = rs;

        clsize = 1
        clreads = $2","$3","$4","$5","$6","$9","$10","$11","$12","$1
        chr = $2
    }
    else
    {

        if ($3 <= (min_ls + maxIsize))
        {
            reason=""
            clsize += 1
            clreads = $2","$3","$4","$5","$6","$9","$10","$11","$12","$1"\t"clreads
            Lstarts[clsize] = $3

            # Update the minimum RS value if necessary
            if ($9 < min_rs) {
              min_rs = $9
            }
        }
        else #New cluster
        {
            if ( clsize >= numEvertInsertCutoff)
            {
                print chr","min_ls","min_rs, clsize, clreads > resF
                cls[clsize]++
                retained_cl += 1
            }

            # reset variables
            cl++
            ls = $3;
            min_ls = ls;
            le = $4;
            rs = $9;
            # In a new first cluster, RS is the minimum RS
            min_rs = rs;
            clsize = 1
            clreads = $2","$3","$4","$5","$6","$9","$10","$11","$12","$1
        }
    }

    #print $0, cl, clsize , reason

}
END{
    #last cluster
    if ( clsize >= numEvertInsertCutoff)
    {
                print chr","min_ls","min_rs, clsize, clreads > resF
                cls[clsize]++
                retained_cl += 1
    }

    #summary
    print "#read_pairs", "#clusters", "#retained_cluster"
    print NR, cl, retained_cl

    print "\nclSize\tcount"  > sumaryF
    for (s in cls) print s, cls[s]  > sumaryF
}
' $distFile
}

# Iterate over pools and chromosomes

for chr in `seq 1 19` X
do
  cd Chr$chr

for pool in 1
  do
    step2e  ~/step1/data/Chr${chr}/c${chr}p${pool}e.txt 232 5 ~/step2/data/Chr${chr}/c${chr}p${pool}e.tsv ~/step2/data/Chr${chr}/c${chr}p${pool}e_summary.tsv
  done
for pool in 2
  do
    step2e  ~/step1/data/Chr${chr}/c${chr}p${pool}e.txt 215 5 ~/step2/data/Chr${chr}/c${chr}p${pool}e.tsv ~/step2/data/Chr${chr}/c${chr}p${pool}e_summary.tsv
  done
for pool in 3
  do
    step2e  ~/step1/data/Chr${chr}/c${chr}p${pool}e.txt 247 5 ~/step2/data/Chr${chr}/c${chr}p${pool}e.tsv ~/step2/data/Chr${chr}/c${chr}p${pool}e_summary.tsv
  done
for pool in 4
  do
    step2e  ~/step1/data/Chr${chr}/c${chr}p${pool}e.txt 242 5 ~/step2/data/Chr${chr}/c${chr}p${pool}e.tsv ~/step2/data/Chr${chr}/c${chr}p${pool}e_summary.tsv
  done
for pool in 5
  do
    step2e  ~/step1/data/Chr${chr}/c${chr}p${pool}e.txt 216 5 ~/step2/data/Chr${chr}/c${chr}p${pool}e.tsv ~/step2/data/Chr${chr}/c${chr}p${pool}e_summary.tsv
  done
for pool in 6
  do
    step2e  ~/step1/data/Chr${chr}/c${chr}p${pool}d.txt 246 5 ~/step2/data/Chr${chr}/c${chr}p${pool}e.tsv ~/step2/data/Chr${chr}/c${chr}p${pool}e_summary.tsv
  done
for pool in 7
  do
    step2e  ~/step1/data/Chr${chr}/c${chr}p${pool}e.txt 149 5 ~/step2/data/Chr${chr}/c${chr}p${pool}e.tsv ~/step2/data/Chr${chr}/c${chr}p${pool}e_summary.tsv
  done
  cd ..
done

function step5(){
    comparison=$1

    # Define the pools relevant to the specified comparison

    if [[ ${comparison} -eq 1 ]]; then
        poolA=2
        poolB=3
    elif [[ ${comparison} -eq 2 ]]; then
        poolA=4
        poolB=7
    elif [[ ${comparison} -eq 3 ]]; then
        poolA=2
        poolB=4
    elif [[ ${comparison} -eq 4 ]]; then
        poolA=3
        poolB=7
    elif [[ ${comparison} -eq 5 ]]; then
        poolA=4
        poolB=6
    elif [[ ${comparison} -eq 6 ]]; then
        poolA=2
        poolB=5
    elif [[ ${comparison} -eq 7 ]]; then
        poolA=5
        poolB=6
    elif [[ ${comparison} -eq 8 ]]; then
        poolA=1
        poolB=5
    elif [[ ${comparison} -eq 9 ]]; then
        poolA=1
        poolB=6
    elif [[ ${comparison} -eq 10 ]]; then
    	poolA=2
    	poolB=4
    elif [[ ${comparison} -eq 11 ]]; then
    	poolA=3
    	poolB=7
    fi

	#### DUPLICATIONS (EVERTED READ-CLUSTERS)

	## Subset legitimate duplications (everted reads with consistent directionality)

	awk -F $'\t' 'BEGIN {OFS = FS} {if ($19=="DUP") print $0}' ~/step4pt5/data/comparison${comparison}/S4_everted_c${comparison}.tsv > S4_legitimate_everted_c${comparison}.tsv


	### Quantitative duplications

	awk -F $'\t' 'BEGIN {OFS = FS} {if ($13=="Quantitative") print $0}' S4_legitimate_everted_c${comparison}.tsv > S4_everted_c${comparison}_Quantitative.tsv

	# Cauculate the most extreme 95% of DiffSupIns for legitimate duplications

	sort -n S4_everted_c${comparison}_Quantitative.tsv  | awk '{all[NR] = $11} END{print all[int(NR*0.95)]}' > QuantitativeDSI95.tsv

	# Cauculate the lower 5% of DiffSupIns *for legitimate duplictions*

	sort -n S4_everted_c${comparison}_Quantitative.tsv  | awk '{all[NR] = $11} END{print all[int(NR*0.05)]}' > QuantitativeDSI05.tsv

	# Append the 95th percentile to QuantitativeDSI95.tsv: it is the 20th field

	touch QuantDSI95.tsv
	touch QuantDSI05.tsv


	cat S4_everted_c${comparison}_Quantitative.tsv | while read line
	do
	   cat QuantitativeDSI95.tsv >> QuantDSI95.tsv
	   cat QuantitativeDSI05.tsv >> QuantDSI05.tsv
	done

	paste S4_everted_c${comparison}_Quantitative.tsv QuantDSI95.tsv > S4_everted_c${comparison}_Quantitative_appended.tsv


	# Append the 5th percentile to QuantitativeDSI95.tsv: it is the 21st field

	paste S4_everted_c${comparison}_Quantitative_appended.tsv QuantDSI05.tsv > S4_everted_c${comparison}_Quantitative_appended_lower.tsv

	rm -rf QuantDSI95.tsv
	rm -rf QuantDSI05.tsv

	# Remove rows that exceed the thresholds in the appropriate direction and remove their respective fields

	awk -F $'\t' 'BEGIN {OFS = FS} {if ($18=="DUP_A" && $11 >= $20) print $0}' S4_everted_c${comparison}_Quantitative_appended_lower.tsv > S4_subset_everted_c${comparison}_Quantitative_DeletionA.tsv
	awk -F $'\t' 'BEGIN {OFS = FS} {if ($18=="DUP_B" && $11 <= $21) print $0}' S4_everted_c${comparison}_Quantitative_appended_lower.tsv > S4_subset_everted_c${comparison}_Quantitative_DeletionB.tsv

	cat S4_subset_everted_c${comparison}_Quantitative_DeletionA.tsv S4_subset_everted_c${comparison}_Quantitative_DeletionB.tsv > S4_subset_everted_c${comparison}_Quantitative.tsv

	cut -f1-19 S4_subset_everted_c${comparison}_Quantitative.tsv > S4_everted_c${comparison}_Quantitative_appended2.tsv

	# Furter subset reads that exceed the Read Depth Ratio threshold (upper or lower)
	awk -F $'\t' 'BEGIN {OFS = FS} {if ($12 != 0) print $0}' S4_everted_c${comparison}_Quantitative_appended2.tsv | awk -F $'\t' 'BEGIN {OFS = FS} {if ($18=="DUP_A" && $12 >= $15) print $0}' > S4_everted_c${comparison}_Quantitative_appendedDELA.tsv
	awk -F $'\t' 'BEGIN {OFS = FS} {if ($12 != 0) print $0}' S4_everted_c${comparison}_Quantitative_appended2.tsv | awk -F $'\t' 'BEGIN {OFS = FS} {if ($18=="DUP_B" && $12 <= $16) print $0}' > S4_everted_c${comparison}_Quantitative_appendedDELB.tsv

	cat S4_everted_c${comparison}_Quantitative_appendedDELA.tsv S4_everted_c${comparison}_Quantitative_appendedDELB.tsv > S4_everted_c${comparison}_Quantitative_appended3.tsv

	### Qualitative duplications

	awk -F $'\t' 'BEGIN {OFS = FS} {if ($13=="Qualitative") print $0}' S4_legitimate_everted_c${comparison}.tsv > S4_everted_c${comparison}_Qualitative.tsv

	# Cauculate the most extreme 95% of DiffSupIns for *legitimate duplications*

	sort -n S4_everted_c${comparison}_Qualitative.tsv  | awk '{all[NR] = $11} END{print all[int(NR*0.95)]}' > QualitativeDSI95.tsv

	# Cauculate the lower 5% of DiffSupIns *for legitimate duplictions*

	sort -n S4_everted_c${comparison}_Qualitative.tsv  | awk '{all[NR] = $11} END{print all[int(NR*0.05)]}' > QualitativeDSI05.tsv

	# Append the 95th percentile to QualitativeDSI95.tsv: it is the 20th field

	touch QualDSI95.tsv
	touch QualDSI05.tsv


	cat S4_everted_c${comparison}_Qualitative.tsv | while read line
	do
	   cat QualitativeDSI95.tsv >> QualDSI95.tsv
	   cat QualitativeDSI05.tsv >> QualDSI05.tsv
	done

	paste S4_everted_c${comparison}_Qualitative.tsv QualDSI95.tsv > S4_everted_c${comparison}_Qualitative_appended.tsv

	# Append the 5th percentile to QualitativeDSI95.tsv: it is the 21st field

	paste S4_everted_c${comparison}_Qualitative_appended.tsv QualDSI05.tsv > S4_everted_c${comparison}_Qualitative_appended_lower.tsv

	rm -rf QualDSI95.tsv
	rm -rf QualDSI05.tsv

	# Subset out rows that exceed the thresholds in the appropriate direction and remove their respective fields
	awk -F $'\t' 'BEGIN {OFS = FS} {if ($18=="DUP_A" && $11 >= $20) print $0}' S4_everted_c${comparison}_Qualitative_appended_lower.tsv > S4_subset_everted_c${comparison}_Qualitative_DeletionA.tsv
	awk -F $'\t' 'BEGIN {OFS = FS} {if ($18=="DUP_B" && $11 <= $21) print $0}' S4_everted_c${comparison}_Qualitative_appended_lower.tsv > S4_subset_everted_c${comparison}_Qualitative_DeletionB.tsv

	cat S4_subset_everted_c${comparison}_Qualitative_DeletionA.tsv S4_subset_everted_c${comparison}_Qualitative_DeletionB.tsv > S4_subset_everted_c${comparison}_Qualitative.tsv

	cut -f1-19 S4_subset_everted_c${comparison}_Qualitative.tsv > S4_everted_c${comparison}_Qualitative_appended2.tsv

		# Furter subset reads that exceed the Read Depth Ratio threshold (upper or lower)
	awk -F $'\t' 'BEGIN {OFS = FS} {if ($12 != 0) print $0}' S4_everted_c${comparison}_Qualitative_appended2.tsv | awk -F $'\t' 'BEGIN {OFS = FS} {if ($18=="DUP_A" && $12 >= $15) print $0}' > S4_everted_c${comparison}_Qualitative_appendedDELA.tsv
	awk -F $'\t' 'BEGIN {OFS = FS} {if ($12 != 0) print $0}' S4_everted_c${comparison}_Qualitative_appended2.tsv | awk -F $'\t' 'BEGIN {OFS = FS} {if ($18=="DUP_B" && $12 <= $16) print $0}' > S4_everted_c${comparison}_Qualitative_appendedDELB.tsv

	cat S4_everted_c${comparison}_Qualitative_appendedDELA.tsv S4_everted_c${comparison}_Qualitative_appendedDELB.tsv > S4_everted_c${comparison}_Qualitative_appended3.tsv


	# Collate qualitative and quantitative duplications

	cat S4_everted_c${comparison}_Quantitative_appended3.tsv S4_everted_c${comparison}_Qualitative_appended3.tsv > CNV_everted_subset_c${comparison}.tsv


	# remove intermediate files:
	rm -rf S4_subset_everted_c${comparison}_Qualitative_DeletionA.tsv
	rm -rf S4_subset_everted_c${comparison}_Qualitative_DeletionB.tsv
	rm -rf S4_subset_everted_c${comparison}_Quantitative_DeletionA.tsv
	rm -rf S4_subset_everted_c${comparison}_Quantitative_DeletionB.tsv
	rm -rf S4_everted_c${comparison}_Quantitative_appendedDELA.tsv
	rm -rf S4_everted_c${comparison}_Quantitative_appendedDELB.tsv
	rm -rf S4_everted_c${comparison}_Qualitative_appendedDELA.tsv
	rm -rf S4_everted_c${comparison}_Qualitative_appendedDELB.tsv
	rm -rf QualitativeDSI95.tsv
	rm -rf QualitativeDSI05.tsv
	rm -rf S4_subset_everted_c${comparison}_Qualitative.tsv
	rm -rf S4_everted_c${comparison}_Qualitative_appended_lower.tsv
	rm -rf S4_legitimate_everted_c${comparison}.tsv
	rm -rf S4_everted_c${comparison}_Quantitative_appended_lower.tsv
	rm -rf S4_everted_c${comparison}_Qualitative_appended3.tsv
	rm -rf S4_everted_c${comparison}_Quantitative_appended3.tsv
	rm -rf S4_everted_c${comparison}_Quantitative_appended.tsv
	rm -rf S4_everted_c${comparison}_Quantitative_appended2.tsv
	rm -rf S4_everted_c${comparison}_Qualitative_appended2.tsv
	rm -rf S4_everted_c${comparison}_Qualitative_appended.tsv
	rm -rf QuantitativeDSI05.tsv
	rm -rf QuantitativeDSI95.tsv
	rm -rf QuantDSI05.tsv
	rm -rf S4_everted_c${comparison}.tsv


	#### DELETIONS (DISTANT READ-CLUSTERS)

	## Subset legitimate deletions (distant reads with consistent directionality)

	awk -F $'\t' 'BEGIN {OFS = FS} {if ($19=="DEL") print $0}' ~/step4pt5/data/comparison${comparison}/S4_distant_c${comparison}.tsv > S4_legitimate_distant_c${comparison}.tsv

	### Quantitative deletions

	awk -F $'\t' 'BEGIN {OFS = FS} {if ($13=="Quantitative") print $0}' S4_legitimate_distant_c${comparison}.tsv > S4_distant_c${comparison}_Quantitative.tsv

	# Cauculate the upper 95% of DiffSupIns for legitimate deletions

	sort -n S4_distant_c${comparison}_Quantitative.tsv  | awk '{all[NR] = $11} END{print all[int(NR*0.95)]}' > QuantitativeDSI95.tsv

	# Cauculate the lower 5% of DiffSupIns for legitimate deletions

	sort -n S4_distant_c${comparison}_Quantitative.tsv  | awk '{all[NR] = $11} END{print all[int(NR*0.05)]}' > QuantitativeDSI05.tsv

	# Append the 95th percentile to QuantitativeDSI95.tsv: it is the 20th field

	touch QuantDSI95.tsv
	touch QuantDSI05.tsv

	cat S4_distant_c${comparison}_Quantitative.tsv | while read line
	do
	   cat QuantitativeDSI95.tsv >> QuantDSI95.tsv
	   cat QuantitativeDSI05.tsv >> QuantDSI05.tsv
	done

	paste S4_distant_c${comparison}_Quantitative.tsv QuantDSI95.tsv > S4_distant_c${comparison}_Quantitative_appended.tsv

	# Append the 5th percentile to QuantitativeDSI95.tsv: it is the 21st field

	paste S4_distant_c${comparison}_Quantitative_appended.tsv QuantDSI05.tsv > S4_distant_c${comparison}_Quantitative_appended_lower.tsv

	rm -rf QuantDSI95.tsv
	rm -rf QuantDSI05.tsv


	# Subset out rows that exceed the thresholds in the appropriate direction and remove their respective fields

	# For extreme cases in which there is a *deletion* in poolA, field 11 (read pairs in A - read pairs in B) will be greater
	#	than field 20 (the 95th percentile for field 11).

	# For extreme cases in which there is a *deletion* in pool B, field 11 will be smaller than field 21 (the  5th percentile
	#	of field 11)

	awk -F $'\t' 'BEGIN {OFS = FS} {if ($18=="DEL_A" && $11 >= $20) print $0}' S4_distant_c${comparison}_Quantitative_appended_lower.tsv > S4_subset_distant_c${comparison}_Quantitative_DeletionA.tsv
	awk -F $'\t' 'BEGIN {OFS = FS} {if ($18=="DEL_B" && $11 <= $21) print $0}' S4_distant_c${comparison}_Quantitative_appended_lower.tsv > S4_subset_distant_c${comparison}_Quantitative_DeletionB.tsv

	cat S4_subset_distant_c${comparison}_Quantitative_DeletionA.tsv S4_subset_distant_c${comparison}_Quantitative_DeletionB.tsv > S4_subset_distant_c${comparison}_Quantitative.tsv

	cut -f1-19 S4_subset_distant_c${comparison}_Quantitative.tsv > S4_distant_c${comparison}_Quantitative_appended2.tsv

	# Furter subset reads that exceed the Read Depth Ratio threshold (upper or lower)
	awk -F $'\t' 'BEGIN {OFS = FS} {if ($12 != 0) print $0}' S4_distant_c${comparison}_Quantitative_appended2.tsv | awk -F $'\t' 'BEGIN {OFS = FS} {if ($18 == "DEL_A" && $12 <= $16) print $0}' > S4_distant_c${comparison}_Quantitative_appendedDELA.tsv
	awk -F $'\t' 'BEGIN {OFS = FS} {if ($12 != 0) print $0}' S4_distant_c${comparison}_Quantitative_appended2.tsv | awk -F $'\t' 'BEGIN {OFS = FS} {if ($18 == "DEL_B" && $12 >= $15) print $0}' > S4_distant_c${comparison}_Quantitative_appendedDELB.tsv

	cat S4_distant_c${comparison}_Quantitative_appendedDELA.tsv S4_distant_c${comparison}_Quantitative_appendedDELB.tsv > S4_distant_c${comparison}_Quantitative_appended3.tsv


	### Qualitative distant

	awk -F $'\t' 'BEGIN {OFS = FS} {if ($13=="Qualitative") print $0}' S4_legitimate_distant_c${comparison}.tsv > S4_distant_c${comparison}_Qualitative.tsv

	# Cauculate the upper 95% of DiffSupIns *for legitimate deletions*

	sort -n S4_distant_c${comparison}_Qualitative.tsv  | awk '{all[NR] = $11} END{print all[int(NR*0.95)]}' > QualitativeDSI95.tsv

	# Cauculate the lower 5% of DiffSupIns *for legitimate deletions*

	sort -n S4_distant_c${comparison}_Qualitative.tsv  | awk '{all[NR] = $11} END{print all[int(NR*0.05)]}' > QualitativeDSI05.tsv

	# Append the 95th percentile to QualitativeDSI95.tsv: it is the 20th field

	touch QualDSI95.tsv
	touch QualDSI05.tsv

	cat S4_distant_c${comparison}_Qualitative.tsv | while read line
	do
	   cat QualitativeDSI95.tsv >> QualDSI95.tsv
	   cat QualitativeDSI05.tsv >> QualDSI05.tsv
	done

	paste S4_distant_c${comparison}_Qualitative.tsv QualDSI95.tsv > S4_distant_c${comparison}_Qualitative_appended.tsv

	# Append the 5th percentile to QualitativeDSI95.tsv: it is the 21st field

	paste S4_distant_c${comparison}_Qualitative_appended.tsv QualDSI05.tsv > S4_distant_c${comparison}_Qualitative_appended_lower.tsv

	rm -rf QualDSI95.tsv
	rm -rf QualDSI05.tsv


	# Subset out rows that exceed the thresholds in the appropriate direction and remove their respective fields

	# For extreme cases in which there is a *deletion* in poolA, field 11 (read pairs in A - read pairs in B) will be greater
	#	than field 20 (the 95th percentile for field 11).

	# For extreme cases in which there is a *deletion* in pool B, field 11 will be smaller than field 21 (the  5th percentile
	#	of field 11)

	awk -F $'\t' 'BEGIN {OFS = FS} {if ($18=="DEL_A" && $11 >= $20) print $0}' S4_distant_c${comparison}_Qualitative_appended_lower.tsv > S4_subset_distant_c${comparison}_Qualitative_DeletionA.tsv
	awk -F $'\t' 'BEGIN {OFS = FS} {if ($18=="DEL_B" && $11 <= $21) print $0}' S4_distant_c${comparison}_Qualitative_appended_lower.tsv > S4_subset_distant_c${comparison}_Qualitative_DeletionB.tsv

	cat S4_subset_distant_c${comparison}_Qualitative_DeletionA.tsv S4_subset_distant_c${comparison}_Qualitative_DeletionB.tsv > S4_subset_distant_c${comparison}_Qualitative.tsv

	cut -f1-19 S4_subset_distant_c${comparison}_Qualitative.tsv > S4_distant_c${comparison}_Qualitative_appended2.tsv

	# Furter subset reads that exceed the Read Depth Ratio threshold (upper or lower)
	awk -F $'\t' 'BEGIN {OFS = FS} {if ($12 != 0) print $0}' S4_distant_c${comparison}_Qualitative_appended2.tsv | awk -F $'\t' 'BEGIN {OFS = FS} {if ($18 == "DEL_A" && $12 <= $16) print $0}' > S4_distant_c${comparison}_Qualitative_appendedDELA.tsv
	awk -F $'\t' 'BEGIN {OFS = FS} {if ($12 != 0) print $0}' S4_distant_c${comparison}_Qualitative_appended2.tsv | awk -F $'\t' 'BEGIN {OFS = FS} {if ($18 == "DEL_B" && $12 >= $15) print $0}' > S4_distant_c${comparison}_Qualitative_appendedDELB.tsv

	## collate distant qualitative reads with deletions in pool A and those with deletions in pool B
	cat S4_distant_c${comparison}_Qualitative_appendedDELA.tsv S4_distant_c${comparison}_Qualitative_appendedDELB.tsv > S4_distant_c${comparison}_Qualitative_appended3.tsv

	## collate qualitative and quantitative distant reads
	cat S4_distant_c${comparison}_Quantitative_appended3.tsv S4_distant_c${comparison}_Qualitative_appended3.tsv > CNV_distant_subset_c${comparison}.tsv

	#### Collate all subset CNVs

	cat CNV_distant_subset_c${comparison}.tsv CNV_everted_subset_c${comparison}.tsv > CNV_comparison${comparison}.tsv

	# remove intermediate files:
	rm -rf S4_subset_distant_c${comparison}_Qualitative_DeletionA.tsv
	rm -rf S4_subset_distant_c${comparison}_Qualitative_DeletionB.tsv
	rm -rf S4_subset_distant_c${comparison}_Quantitative_DeletionA.tsv
	rm -rf S4_subset_distant_c${comparison}_Quantitative_DeletionB.tsv
	rm -rf S4_distant_c${comparison}_Quantitative_appendedDELA.tsv
	rm -rf S4_distant_c${comparison}_Quantitative_appendedDELB.tsv
	rm -rf S4_distant_c${comparison}_Qualitative_appendedDELA.tsv
	rm -rf S4_distant_c${comparison}_Qualitative_appendedDELB.tsv
	rm -rf QualitativeDSI95.tsv
	rm -rf QualitativeDSI05.tsv
	rm -rf S4_subset_distant_c${comparison}_Qualitative.tsv
	rm -rf S4_distant_c${comparison}_Qualitative_appended_lower.tsv
	rm -rf S4_legitimate_distant_c${comparison}.tsv
	rm -rf S4_distant_c${comparison}_Quantitative_appended_lower.tsv
	rm -rf S4_distant_c${comparison}_Qualitative_appended3.tsv
	rm -rf S4_distant_c${comparison}_Quantitative_appended3.tsv
	rm -rf S4_distant_c${comparison}_Quantitative_appended.tsv
	rm -rf S4_distant_c${comparison}_Quantitative_appended2.tsv
	rm -rf S4_distant_c${comparison}_Qualitative_appended2.tsv
	rm -rf S4_distant_c${comparison}_Qualitative_appended.tsv
	rm -rf QuantitativeDSI05.tsv
	rm -rf QuantitativeDSI95.tsv
	rm -rf QuantDSI05.tsv
	rm -rf S4_distant_c${comparison}.tsv
	rm -rf S4_distant_c${comparison}_Qualitative.tsv
	rm -rf S4_distant_c${comparison}_Quantitative.tsv
	rm -rf S4_subset_distant_c${comparison}_Quantitative.tsv
}

## focal comparisons
# comparison 1
for comparison in 1
do
 cd ~/step5/data/comparison1
 step5 $comparison
done
cd ~

# comparison 2
for comparison in 2
do
 cd ~/step5/data/comparison2
 step5 $comparison
done
cd ~

## control comparisons

# comparison 3
for comparison in 3
do
 cd ~/step5/data/comparison3
 step5 $comparison
done
cd ~

# comparison 4
for comparison in 4
do
 cd ~/step5/data/comparison4
 step5 $comparison
done
cd ~

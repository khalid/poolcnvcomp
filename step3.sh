function step3() {

# Input parameters: chromosome and comparison
chr=$1
comparison=$2

# Pools A and B and defined by the comparison.
# e.g., comparison 1 compairs pools 2 and 3.

if [[ ${comparison} -eq 1 ]]; then
  poolA=2
  poolB=3
elif [[ ${comparison} -eq 2 ]]; then
  poolA=4
  poolB=7
elif [[ ${comparison} -eq 3 ]]; then
  poolA=2
  poolB=4
elif [[ ${comparison} -eq 4 ]]; then
  poolA=3
  poolB=7
elif [[ ${comparison} -eq 5 ]]; then
  poolA=4
  poolB=6
elif [[ ${comparison} -eq 6 ]]; then
  poolA=2
  poolB=5
elif [[ ${comparison} -eq 7 ]]; then
  poolA=5
  poolB=6
elif [[ ${comparison} -eq 8 ]]; then
  poolA=1
  poolB=5
elif [[ ${comparison} -eq 9 ]]; then
  poolA=1
  poolB=6
fi

# Calculate the normalisation constant.
# dA and dB are calculated in step 1
dA=$(cat ~/step1/data/Chr${chr}/d${poolA}_c${chr}.txt) # Pool A = pool 1; specify d1_Chr${chr}
dB=$(cat ~/step1/data/Chr${chr}/d${poolB}_c${chr}.txt) # Pool A = pool 6; specify d5_Chr${chr}
A_B=$(echo $dA+$dB | bc)
mean=$(echo $A_B/2 | bc)
normA=$(echo $mean/$dA | bc)
normB=$(echo $mean/$dB | bc)


normConstA=$normA
normConstB=$normB

# Tandem duplications
python step3_combineEvertedClustersAcrossPoolsHN.py  ~/step2/data/Chr$chr/c${chr}p${poolA}e.tsv ~/step2/data/Chr${chr}/c${chr}p${poolB}e.tsv $normConstA $normConstB > ~/step3/data/comparison${comparison}/c${chr}P${poolA}vsP${poolB}_everted.tsv

# Deletions
python step3_combineDistantClustersAccrossPoolsHN.py ~/step2/data/Chr$chr/c${chr}p${poolA}e.tsv ~/step2/data/Chr${chr}/c${chr}p${poolB}e.tsv $normConstA $normConstB > ~/step3/data/comparison${comparison}/c${chr}P${poolA}vsP${poolB}_everted.tsv
}

# Iterate over specified chromosomes and comparisons
for chr in `seq 1 19` X
do
  for comparison in `seq 1 2` # comparisons 1 and 2 only in this case
  do
    step3 $chr $comparison
  done
done

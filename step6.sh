###################### Focal Test ######################

# Assume the following subdirectories within ~/step6/data/ : focal/ control/ final/

cd ~/step6/data/focal

#### Deletions

## First, sort deletions
awk 'BEGIN{OFS="\t"}{split($1, coords, ","); print coords[1], coords[2],
coords[3], $0 }' ~/step5/data/comparison1/CNV_distant_subset_c1.tsv | sort -k 1,1 -k2,2n > CNVs_DEL_c1.tsv

awk 'BEGIN{OFS="\t"}{split($1, coords, ","); print coords[1], coords[2],
coords[3], $0 }' ~/step5/data/comparison2/CNV_distant_subset_c2.tsv | sort -k 1,1 -k2,2n > CNVs_DEL_c2.tsv

## Second, implement non-stringent clustering: overlap + 95% size similarity.

# Require overlap:
# (here, bedtools v2.27.1)
bedtools closest -a CNVs_DEL_c1.tsv -b CNVs_DEL_c2.tsv -d | awk 'BEGIN{OFS="\t"} \
{if ($NF < 1) print}' | awk '{if ($21==$43) print $0 }' | awk '!a[$4]++' | awk '!a[$26]++' > DELc1vsc2_Str_overlapOnly.tsv

# Make the 46th field the ratio of CNV sizes:
awk '{print $12/$32}' DELc1vsc2_Str_overlapOnly.tsv > SizeDiffDEL_Str.tsv
paste DELc1vsc2_Str_overlapOnly.tsv SizeDiffDEL_Str.tsv > DELc1vsc2_intermediate2_Str.tsv

# Implement non-stringent clustering: require absolute overlap and event size similarity of 5%
awk '{if ($46 < 1.05) print}' DELc1vsc2_intermediate2_Str.tsv > DELc1vsc2_intermediate3_Str.tsv
awk '{if ($46 > 0.95) print}' DELc1vsc2_intermediate3_Str.tsv > DELc1vsc2_95.tsv

## Third, implement stringent clustering (IBS): requiring absolute overlap and identical coordinates.
## But first we have to extract coordinate information in a format that Bedtools can read:

# This information is in fields 1 and 26 (comparisons 1 and 2):
cut -f4 DELc1vsc2_95.tsv > DEL_focal_comp1_comma.tsv
cut -f26 DELc1vsc2_95.tsv > DEL_focal_comp2_comma.tsv

# Replace comma separators with tab separators
cut -d ',' -f1 DEL_focal_comp1_comma.tsv > DEL_focal_comp1_chr.tsv
cut -d ',' -f2 DEL_focal_comp1_comma.tsv > DEL_focal_comp1_start.tsv
cut -d ',' -f3 DEL_focal_comp1_comma.tsv > DEL_focal_comp1_end.tsv

cut -d ',' -f1 DEL_focal_comp2_comma.tsv > DEL_focal_comp2_chr.tsv
cut -d ',' -f2 DEL_focal_comp2_comma.tsv > DEL_focal_comp2_start.tsv
cut -d ',' -f3 DEL_focal_comp2_comma.tsv > DEL_focal_comp2_end.tsv

# Re-assemble coordinate information in a way that Bedtools can read
paste DEL_focal_comp1_chr.tsv DEL_focal_comp1_start.tsv DEL_focal_comp1_end.tsv > DEL_focal_comp1.tsv
paste DEL_focal_comp2_chr.tsv DEL_focal_comp2_start.tsv DEL_focal_comp2_end.tsv > DEL_focal_comp2.tsv

# Sort these
sort -k 1,1 -k2,2n  DEL_focal_comp1.tsv > DEL_focal_comp1_sorted.tsv
sort -k 1,1 -k2,2n  DEL_focal_comp2.tsv > DEL_focal_comp2_sorted.tsv

# match the CNVs using Bedtools -d
# When a feature in B overlaps a feature in A, a distance of 0 is reported

bedtools closest -a DEL_focal_comp1_sorted.tsv -b DEL_focal_comp2_sorted.tsv -d | awk 'BEGIN{OFS="\t"} \
{if ($NF < 1) print}' > DELc1vsc2_Str_overlapOnly.tsv

# Calculate start and end differences

awk '{print ($2-$5)}' DELc1vsc2_Str_overlapOnly.tsv > startDiff.tsv
awk '{print ($3-$6)}' DELc1vsc2_Str_overlapOnly.tsv > endDiff.tsv

## Here, the columns will not be 2 and 6. Read in DELc1vsc2_Str_overlapOnly.tsv and extract the start and end
##  differences from the file.

paste DELc1vsc2_Str_overlapOnly.tsv startDiff.tsv endDiff.tsv > DELc1vsc2_appended.tsv

# paste TEST.tsv startDiff.tsv endDiff.tsv > DELc1vsc2_appended.tsv

# Append IBS status
awk -v OFS='\t' '{if ($8==0 && $9==0) print $0,"IBS"}' DELc1vsc2_appended.tsv > DELc1vsc2_IBS.tsv

# Remember: IBS CNVs are a subset of 95% size-similar ('95' hereafter) CNVs
# IBS deletions are listed in DELc1vsc2_IBS.tsv
# 95 deletions are listed in DELc1vsc2_95.tsv
# So DELc1vsc2_IBS.tsv is a subset of DELc1vsc2_95.tsv
# This means deletions in DELc1vsc2_95.tsv that are NOT in DELc1vsc2_IBS.tsv are non_IBS
# To find non_IBS deletions, use grep -vf
# HOWEVER each DEL contains two pieces of information: the coordintes in the first test and the coordinates in the second
# So first we need a universal matching system of three key pieces of information:
# (1) the chromosome the DEL occurs on
# (2) the minimum value of the start coordiante from either the first or second test
# (3) the maximum value of the end coordiante from either the first or second test
# These then must be combined into an underscore-separated matching system


# Make a unique indentifier for each of the 95 deletions:

awk '{if ($2 >= $24) print $24; else if ($24 >= $2) print $2}'  DELc1vsc2_95.tsv > focal_95_DEL_minStart.tsv
awk '{if ($3 >= $25) print $3; else if ($3 <= $25) print $25}' DELc1vsc2_95.tsv > focal_95_DEL_maxEnd.tsv
cut -f1 DELc1vsc2_95.tsv > focal_95_DEL_chr.tsv
paste -d _ focal_95_DEL_chr.tsv focal_95_DEL_minStart.tsv focal_95_DEL_maxEnd.tsv > focal_95_DEL_identifier.tsv


# Extract the matcing indicies from IBS deletions:
# Note that this is redundant in the case of IBS deltions, since $2==$5 and $3 == $6 in all cases.
# But we will use the same principle for consistency

awk '{if ($2 >= $5) print $5; else if ($5 >= $2) print $2}' DELc1vsc2_IBS.tsv > focal_IBS_DEL_minStart.tsv
awk '{if ($3 >= $6) print $3; else if ($3 <= $6) print $6}' DELc1vsc2_IBS.tsv > focal_IBS_DEL_maxEnd.tsv
cut -f1 DELc1vsc2_IBS.tsv > focal_IBS_DEL_chr.tsv

# Combine these into a single identifier connected with an underscore:

paste -d _ focal_IBS_DEL_chr.tsv focal_IBS_DEL_minStart.tsv focal_IBS_DEL_maxEnd.tsv > focal_IBS_DEL_identifier.tsv

# Generate a reduced summary file of IBS deletions:

paste focal_IBS_DEL_identifier.tsv DELc1vsc2_IBS.tsv > DELc1vsc2_IBS_intermediate.tsv
cut -f1,11 DELc1vsc2_IBS_intermediate.tsv > DELc1vsc2_IBS_indexed.tsv
sort DELc1vsc2_IBS_indexed.tsv > DELc1vsc2_IBS_indexed_sorted.tsv

# now find the deletions that ARE 95% similar in size but are NOT IBS:
# Since focal_IBS_DEL_identifier.tsv is an element of focal_95_DEL_identifier.tsv,
# we want to find the deletions thar occur in the latter but not the former:

# first we need to sort these:

sort focal_IBS_DEL_identifier.tsv > focal_IBS_DEL_identifier_sorted.tsv
sort focal_95_DEL_identifier.tsv > focal_95_DEL_identifier_sorted.tsv


grep -F -x -v -f <(grep -o '[^/]*$' focal_IBS_DEL_identifier_sorted.tsv) <(grep -o '[^/]*$' focal_95_DEL_identifier_sorted.tsv) > focal_nonIBS_identifier.tsv


# append non_IBS status to focal_nonIBS_identifier.tsv , then sort it:
awk -v OFS='\t' '{print $0,"non_IBS"}' focal_nonIBS_identifier.tsv > DELc1vsc2_nonIBS_indexed.tsv
sort DELc1vsc2_nonIBS_indexed.tsv > DELc1vsc2_nonIBS_indexed_sorted.tsv

# Now we can assemble an indexed list of all deletions identified in the focal comparison, categorised as IBS and nonIBS

cat DELc1vsc2_nonIBS_indexed_sorted.tsv DELc1vsc2_IBS_indexed_sorted.tsv > DELc1vsc2_indexed.tsv
sort DELc1vsc2_indexed.tsv > DELc1vsc2_indexed_sorted.tsv

# From here we can use the unique identifer to append this information to the full summary file of deletions from the focal comparison:

# So, first, add the unique identifier to that output file:

paste focal_95_DEL_identifier.tsv DELc1vsc2_95.tsv > DELc1vsc2_95_indexed.tsv
sort DELc1vsc2_95_indexed.tsv > DELc1vsc2_95_indexed_sorted.tsv

# Merge DELc1vsc2_95_indexed.tsv with DELc1vsc2_indexed_sorted.tsv
# Specifically: match the first column of both files, then print all fields of file 1, then print the second field of file 2
join -1 1 -2 1 -t $'\t' DELc1vsc2_95_indexed_sorted.tsv DELc1vsc2_indexed_sorted.tsv > focal_DEL_appended.tsv

# so focal_DEL_appended.tsv is the total list of deletions identified in the focal test
# During the 6 steps we have amassed 48 columns of information for each CNV event,
# many of which are not particularly useful or interesting at this stage.
# The following guide can be used to subset relevant information:

### The output columns are as follows:

# 1: unique identifier for the CNV
## Columns 2-23 refer to the first comparison of the test:
# 2: Chromosome
# 3: Start position
# 4: end position
# 5: Coordinates of the CNV cluster in the first pool of the first comparison
# 6: The coordinates of the cluster found in pool 1 (“NA” if the cluster was only found in pool 2 of the first comparison)
# 7: The (corrected) number of read pairs supporting the event in pool 1 (0 if only found in pool 2of the first comparison)
# 8: Coordinates of the CNV cluster in the second pool of the first comparison
# 9: The (corrected) number of read pairs supporting the event in pool 2 (0 if only found in pool 1 of the first comparison)
# 10: Read depth in pool 1 of comparison 1
# 11: Unmasked CNV size for comparison 1
# 12: Read depth in pool 2 of comparison 1
# 13: Unmasked CNV size for comparison 1(repeated)
# 14: event size window category
# 15: difference in number of supporting inserts (DiffSuppIns)
# 16: Read depth ratio
# 17: Qualitative vs Quantitative difference between pools (“qualitative” if present in one behaviroual class and absent in the other)
# 18: For deletions, are there at least 5 supporting inserts (T/F)
# 19: Upper 95% read-depth ratio threshold for a window of that class (column 13)
# 20: Lower 95 % read-depth ratio threshold for a window of that class (column 13)
# 21: CNV clusters: distant/everted
# 22: Which pool did the CNV occur in? (A=pool 1, e.g. in comparison 1 poolA==2)
# 23: DEL/DUP

## Columns 24-47 refer to the second comparison of the test:
# 24:Chromosome
# 25: start position
# 26: end position
# 27: Coordinates of the CNV cluster in the first pool of the second comparison
# 28: The coordinates of the cluster found in pool 1 (“NA” if the cluster was only found in pool 2 of the second comparison)
# 29: The (corrected) number of read pairs supporting the event in pool 1 (0 if only found in pool 2 of the second comparison)
# 30: Coordinates of the CNV cluster in the second pool of the second comparison
# 31: The (corrected) number of read pairs supporting the event in pool 2 (0 if only found in pool 1 of the second comparison)
# 32: Read depth in pool 1 of comparison 2
# 33: Unmasked CNV size for comparison 2
# 34: Read depth in pool 2 of comparison 2
# 35: Unmasked CNV size for comparison 2 (reapeated)
# 36: event size window category
# 37: difference in number of supporting inserts (DiffSuppIns)
# 38: Read depth ratio in comparison 2
# 39: Qualitative vs Quantitative difference between pools (“qualitative” if present in one behaviroual class and absent in the other)
# 40: For deletions, are there at least 5 supporting inserts (T/F)
# 41: Upper 95% read-depth ratio threshold for a window of that class (column 13)
# 42: Lower 95 % read-depth ratio threshold for a window of that class (column 13)
# 43: CNV clusters: distant/everted
# 44: Which pool did the CNV occur in? (A=pool 1, e.g. in comparison 1 poolA==2)
# 45: DEL/DUP

# 46: bedtools -d output.This value will be 0 if the CNVs in the comparison overlap
# 47: ratio of CNV sizes between the two pools
# 48: IBS (the CNV is identical by state in both pools of the comparison) vs non_IBS (the CNV is not identical by state in both pools of the comparison)

# Finally, we can remove all the intermediate files we generated:
rm -rf CNVs_DEL_c1.tsv
rm -rf CNVs_DEL_c2.tsv
rm -rf DELc1vsc2_Str_overlapOnly.tsv
rm -rf SizeDiffDEL_Str.tsv
rm -rf DELc1vsc2_intermediate2_Str.tsv
rm -rf DELc1vsc2_intermediate3_Str.tsv
rm -rf DELc1vsc2_95.tsv
rm -rf DEL_focal_comp1_comma.tsv
rm -rf DEL_focal_comp2_comma.tsv
rm -rf DEL_focal_comp1_chr.tsv
rm -rf DEL_focal_comp1_start.tsv
rm -rf DEL_focal_comp1_end.tsv
rm -rf DEL_focal_comp2_chr.tsv
rm -rf DEL_focal_comp2_start.tsv
rm -rf DEL_focal_comp2_end.tsv
rm -rf DEL_focal_comp1.tsv
rm -rf DEL_focal_comp2.tsv
rm -rf DEL_focal_comp1_sorted.tsv
rm -rf DEL_focal_comp2_sorted.tsv
rm -rf startDiff.tsv
rm -rf endDiff.tsv
rm -rf DELc1vsc2_appended.tsv
rm -rf DELc1vsc2_IBS.tsv
rm -rf focal_95_DEL_minStart.tsv
rm -rf focal_95_DEL_maxEnd.tsv
rm -rf focal_95_DEL_chr.tsv
rm -rf focal_95_DEL_identifier.tsv
rm -rf focal_IBS_DEL_minStart.tsv
rm -rf focal_IBS_DEL_maxEnd.tsv
rm -rf focal_IBS_DEL_chr.tsv
rm -rf focal_IBS_DEL_identifier.tsv
rm -rf DELc1vsc2_IBS_intermediate.tsv
rm -rf DELc1vsc2_IBS_indexed.tsv
rm -rf DELc1vsc2_IBS_indexed_sorted.tsv
rm -rf focal_IBS_DEL_identifier_sorted.tsv
rm -rf focal_95_DEL_identifier_sorted.tsv
rm -rf focal_nonIBS_identifier.tsv
rm -rf DELc1vsc2_nonIBS_indexed.tsv
rm -rf DELc1vsc2_nonIBS_indexed_sorted.tsv
rm -rf DELc1vsc2_indexed.tsv
rm -rf DELc1vsc2_indexed_sorted.tsv
rm -rf DELc1vsc2_95_indexed.tsv
rm -rf DELc1vsc2_95_indexed_sorted.tsv

## The above script describes the final step for deletions in the Focal Test (two comparisons of choosy vs non-choosy).
# The same principle applies to dupliactions in the Focal Test, as well as duplications and deltions in the Control Test.
# So, I don't include as many comments for the remainder of step 6.

# However, at the end of Step 6 we subtract the CNVs identified in the in the Control Test
# from those identified in the Focal Test.


#### Duplications

### Read in and sort duplications
awk 'BEGIN{OFS="\t"}{split($1, coords, ","); print coords[1], coords[2],
coords[3], $0 }' ~/step5/data/comparison1/CNV_everted_subset_c1.tsv | sort -k 1,1 -k2,2n > CNVs_DUP_c1.tsv

awk 'BEGIN{OFS="\t"}{split($1, coords, ","); print coords[1], coords[2],
coords[3], $0 }' ~/step5/data/comparison2/CNV_everted_subset_c2.tsv | sort -k 1,1 -k2,2n > CNVs_DUP_c2.tsv

### Implement non-stringent clustering: overlap + 95% size similarity.
bedtools closest -a CNVs_DUP_c1.tsv -b CNVs_DUP_c2.tsv -d | awk 'BEGIN{OFS="\t"} \
{if ($NF < 1) print}' | awk '{if ($21==$43) print $0 }' | awk '!a[$4]++' | awk '!a[$26]++' > DUPc1vsc2_Str_overlapOnly.tsv

# Make the 46th field the ratio of CNV sizes:
awk '{print $12/$32}' DUPc1vsc2_Str_overlapOnly.tsv > SizeDiffDUP_Str.tsv
paste DUPc1vsc2_Str_overlapOnly.tsv SizeDiffDUP_Str.tsv > DUPc1vsc2_intermediate2_Str.tsv

# Implement non-stringent clustering: require absolute overlap and event size similarity of 5%
awk '{if ($46 < 1.05) print}' DUPc1vsc2_intermediate2_Str.tsv > DUPc1vsc2_intermediate3_Str.tsv
awk '{if ($46 > 0.95) print}' DUPc1vsc2_intermediate3_Str.tsv > DUPc1vsc2_95.tsv

# Extract, sort & reassemble coordinate infromation so it can be read by bedtools
cut -f4 DUPc1vsc2_95.tsv > DUP_focal_comp1_comma.tsv
cut -f26 DUPc1vsc2_95.tsv > DUP_focal_comp2_comma.tsv

cut -d ',' -f1 DUP_focal_comp1_comma.tsv > DUP_focal_comp1_chr.tsv
cut -d ',' -f2 DUP_focal_comp1_comma.tsv > DUP_focal_comp1_start.tsv
cut -d ',' -f3 DUP_focal_comp1_comma.tsv > DUP_focal_comp1_end.tsv

cut -d ',' -f1 DUP_focal_comp2_comma.tsv > DUP_focal_comp2_chr.tsv
cut -d ',' -f2 DUP_focal_comp2_comma.tsv > DUP_focal_comp2_start.tsv
cut -d ',' -f3 DUP_focal_comp2_comma.tsv > DUP_focal_comp2_end.tsv

paste DUP_focal_comp1_chr.tsv DUP_focal_comp1_start.tsv DUP_focal_comp1_end.tsv > DUP_focal_comp1.tsv
paste DUP_focal_comp2_chr.tsv DUP_focal_comp2_start.tsv DUP_focal_comp2_end.tsv > DUP_focal_comp2.tsv

sort -k 1,1 -k2,2n  DUP_focal_comp1.tsv > DUP_focal_comp1_sorted.tsv
sort -k 1,1 -k2,2n  DUP_focal_comp2.tsv > DUP_focal_comp2_sorted.tsv

# match  CNVs and retain only those that overlap
bedtools closest -a DUP_focal_comp1_sorted.tsv -b DUP_focal_comp2_sorted.tsv -d | awk 'BEGIN{OFS="\t"} \
{if ($NF < 1) print}' > DUPc1vsc2_Str_overlapOnly.tsv

# Calculate start and end differences; append this information
awk '{print ($2-$5)}' DUPc1vsc2_Str_overlapOnly.tsv > startDiff.tsv
awk '{print ($3-$6)}' DUPc1vsc2_Str_overlapOnly.tsv > endDiff.tsv
paste DUPc1vsc2_Str_overlapOnly.tsv startDiff.tsv endDiff.tsv > DUPc1vsc2_appended.tsv

# Append IBS status
awk -v OFS='\t' '{if ($8==0 && $9==0) print $0,"IBS"}' DUPc1vsc2_appended.tsv > DUPc1vsc2_IBS.tsv

# Generate unique identifiers:

awk '{if ($2 >= $24) print $24; else if ($24 >= $2) print $2}'  DUPc1vsc2_95.tsv > focal_95_DUP_minStart.tsv
awk '{if ($3 >= $25) print $3; else if ($3 <= $25) print $25}' DUPc1vsc2_95.tsv > focal_95_DUP_maxEnd.tsv
cut -f1 DUPc1vsc2_95.tsv > focal_95_DUP_chr.tsv
paste -d _ focal_95_DUP_chr.tsv focal_95_DUP_minStart.tsv focal_95_DUP_maxEnd.tsv > focal_95_DUP_identifier.tsv

awk '{if ($2 >= $5) print $5; else if ($5 >= $2) print $2}' DUPc1vsc2_IBS.tsv > focal_IBS_DUP_minStart.tsv
awk '{if ($3 >= $6) print $3; else if ($3 <= $6) print $6}' DUPc1vsc2_IBS.tsv > focal_IBS_DUP_maxEnd.tsv
cut -f1 DUPc1vsc2_IBS.tsv > focal_IBS_DUP_chr.tsv
paste -d _ focal_IBS_DUP_chr.tsv focal_IBS_DUP_minStart.tsv focal_IBS_DUP_maxEnd.tsv > focal_IBS_DUP_identifier.tsv

# Generate a reduced summary file of IBS DUPetions:

paste focal_IBS_DUP_identifier.tsv DUPc1vsc2_IBS.tsv > DUPc1vsc2_IBS_intermediate.tsv
cut -f1,11 DUPc1vsc2_IBS_intermediate.tsv > DUPc1vsc2_IBS_indexed.tsv
sort DUPc1vsc2_IBS_indexed.tsv > DUPc1vsc2_IBS_indexed_sorted.tsv

# Use unique identifiers to find duplications that ARE 95% similar in size but are non_IBS:

sort focal_IBS_DUP_identifier.tsv > focal_IBS_DUP_identifier_sorted.tsv
sort focal_95_DUP_identifier.tsv > focal_95_DUP_identifier_sorted.tsv
grep -F -x -v -f <(grep -o '[^/]*$' focal_IBS_DUP_identifier_sorted.tsv) <(grep -o '[^/]*$' focal_95_DUP_identifier_sorted.tsv) > focal_nonIBS_identifier.tsv

# In this particular case, no DUPs are IBD and therefore DUPc1vsc2_95_indexed_sorted.tsv represents the final list:

# append non_IBS status to focal_nonIBS_identifier.tsv , then sort it:
awk -v OFS='\t' '{print $0,"non_IBS"}' focal_nonIBS_identifier.tsv > DUPc1vsc2_nonIBS_indexed.tsv
sort DUPc1vsc2_nonIBS_indexed.tsv > DUPc1vsc2_nonIBS_indexed_sorted.tsv

# Assemble a list of all IBS and non_IBD duplications identified in the focal comparison
cat DUPc1vsc2_nonIBS_indexed_sorted.tsv DUPc1vsc2_IBS_indexed_sorted.tsv > DUPc1vsc2_indexed.tsv
sort DUPc1vsc2_indexed.tsv > DUPc1vsc2_indexed_sorted.tsv

# Add a unique identifer to this list, then sort it
paste focal_95_DUP_identifier.tsv DUPc1vsc2_95.tsv > DUPc1vsc2_95_indexed.tsv
sort DUPc1vsc2_95_indexed.tsv > DUPc1vsc2_95_indexed_sorted.tsv

# Merge lists to append the final list of all duplications identified in the focal comparison
#join -1 1 -2 1 -t $'\t' DUPc1vsc2_95_indexed_sorted.tsv DUPc1vsc2_indexed_sorted.tsv > focal_DUP_appended.tsv

# In this particular case, no DUPs are IBD and therefore DUPc1vsc2_95_indexed_sorted.tsv represents the final list:
awk -v OFS='\t' '{print $0,"non_IBS"}' DUPc1vsc2_95_indexed_sorted.tsv > focal_DUP_appended.tsv

# Remove intermediate files:
rm -rf CNVs_DUP_c1.tsv
rm -rf CNVs_DUP_c2.tsv
rm -rf DUPc1vsc2_Str_overlapOnly.tsv
rm -rf SizeDiffDUP_Str.tsv
rm -rf DUPc1vsc2_intermediate2_Str.tsv
rm -rf DUPc1vsc2_intermediate3_Str.tsv
rm -rf DUPc1vsc2_95.tsv
rm -rf DUP_focal_comp1_comma.tsv
rm -rf DUP_focal_comp2_comma.tsv
rm -rf DUP_focal_comp1_chr.tsv
rm -rf DUP_focal_comp1_start.tsv
rm -rf DUP_focal_comp1_end.tsv
rm -rf DUP_focal_comp2_chr.tsv
rm -rf DUP_focal_comp2_start.tsv
rm -rf DUP_focal_comp2_end.tsv
rm -rf DUP_focal_comp1.tsv
rm -rf DUP_focal_comp2.tsv
rm -rf DUP_focal_comp1_sorted.tsv
rm -rf DUP_focal_comp2_sorted.tsv
rm -rf startDiff.tsv
rm -rf endDiff.tsv
rm -rf DUPc1vsc2_appended.tsv
rm -rf DUPc1vsc2_IBS.tsv
rm -rf focal_95_DUP_minStart.tsv
rm -rf focal_95_DUP_maxEnd.tsv
rm -rf focal_95_DUP_chr.tsv
rm -rf focal_95_DUP_identifier.tsv
rm -rf focal_IBS_DUP_minStart.tsv
rm -rf focal_IBS_DUP_maxEnd.tsv
rm -rf focal_IBS_DUP_chr.tsv
rm -rf focal_IBS_DUP_identifier.tsv
rm -rf DUPc1vsc2_IBS_intermediate.tsv
rm -rf DUPc1vsc2_IBS_indexed.tsv
rm -rf DUPc1vsc2_IBS_indexed_sorted.tsv
rm -rf focal_IBS_DUP_identifier_sorted.tsv
rm -rf focal_95_DUP_identifier_sorted.tsv
rm -rf focal_nonIBS_identifier.tsv
rm -rf DUPc1vsc2_nonIBS_indexed.tsv
rm -rf DUPc1vsc2_nonIBS_indexed_sorted.tsv
rm -rf DUPc1vsc2_indexed.tsv
rm -rf DUPc1vsc2_indexed_sorted.tsv
rm -rf DUPc1vsc2_95_indexed.tsv
rm -rf DUPc1vsc2_95_indexed_sorted.tsv

###################### Control Test ######################

cd ~/step6/data/focal

#### Deletions

### Read in and sort duplications
awk 'BEGIN{OFS="\t"}{split($1, coords, ","); print coords[1], coords[2],
coords[3], $0 }' ~/step5/data/comparison3/CNV_distant_subset_c3.tsv | sort -k 1,1 -k2,2n > CNVs_DEL_c3.tsv

awk 'BEGIN{OFS="\t"}{split($1, coords, ","); print coords[1], coords[2],
coords[3], $0 }' ~/step5/data/comparison4/CNV_distant_subset_c4.tsv | sort -k 1,1 -k2,2n > CNVs_DEL_c4.tsv

### Implement non-stringent clustering: overlap + 95% size similarity.
bedtools closest -a CNVs_DEL_c3.tsv -b CNVs_DEL_c4.tsv -d | awk 'BEGIN{OFS="\t"} \
{if ($NF < 1) print}' | awk '{if ($21==$43) print $0 }' | awk '!a[$4]++' | awk '!a[$26]++' > DELc3vsc4_Str_overlapOnly.tsv

# Make the 46th field the ratio of CNV sizes:
awk '{print $12/$32}' DELc3vsc4_Str_overlapOnly.tsv > SizeDiffDEL_Str.tsv
paste DELc3vsc4_Str_overlapOnly.tsv SizeDiffDEL_Str.tsv > DELc3vsc4_intermediate2_Str.tsv

# Implement non-stringent clustering: require absolute overlap and event size similarity of 5%
awk '{if ($46 < 1.05) print}' DELc3vsc4_intermediate2_Str.tsv > DELc3vsc4_intermediate3_Str.tsv
awk '{if ($46 > 0.95) print}' DELc3vsc4_intermediate3_Str.tsv > DELc3vsc4_95.tsv

# Extract, sort & reassemble coordinate infromation so it can be read by bedtools
cut -f4 DELc3vsc4_95.tsv > DEL_control_comp3_comma.tsv
cut -f26 DELc3vsc4_95.tsv > DEL_control_comp4_comma.tsv

cut -d ',' -f1 DEL_control_comp3_comma.tsv > DEL_control_comp3_chr.tsv
cut -d ',' -f2 DEL_control_comp3_comma.tsv > DEL_control_comp3_start.tsv
cut -d ',' -f3 DEL_control_comp3_comma.tsv > DEL_control_comp3_end.tsv

cut -d ',' -f1 DEL_control_comp4_comma.tsv > DEL_control_comp4_chr.tsv
cut -d ',' -f2 DEL_control_comp4_comma.tsv > DEL_control_comp4_start.tsv
cut -d ',' -f3 DEL_control_comp4_comma.tsv > DEL_control_comp4_end.tsv

paste DEL_control_comp3_chr.tsv DEL_control_comp3_start.tsv DEL_control_comp3_end.tsv > DEL_control_comp3.tsv
paste DEL_control_comp4_chr.tsv DEL_control_comp4_start.tsv DEL_control_comp4_end.tsv > DEL_control_comp4.tsv

sort -k 1,1 -k2,2n  DEL_control_comp3.tsv > DEL_control_comp3_sorted.tsv
sort -k 1,1 -k2,2n  DEL_control_comp4.tsv > DEL_control_comp4_sorted.tsv

# match  CNVs and retain only those that overlap
bedtools closest -a DEL_control_comp3_sorted.tsv -b DEL_control_comp4_sorted.tsv -d | awk 'BEGIN{OFS="\t"} \
{if ($NF < 1) print}' > DELc3vsc4_Str_overlapOnly.tsv

# Calculate start and end differences; append this information
awk '{print ($2-$5)}' DELc3vsc4_Str_overlapOnly.tsv > startDiff.tsv
awk '{print ($3-$6)}' DELc3vsc4_Str_overlapOnly.tsv > endDiff.tsv
paste DELc3vsc4_Str_overlapOnly.tsv startDiff.tsv endDiff.tsv > DELc3vsc4_appended.tsv

# Append IBS status
awk -v OFS='\t' '{if ($8==0 && $9==0) print $0,"IBS"}' DELc3vsc4_appended.tsv > DELc3vsc4_IBS.tsv

# Generate unique identifiers:
awk '{if ($2 >= $24) print $24; else if ($24 >= $2) print $2}'  DELc3vsc4_95.tsv > control_95_DEL_minStart.tsv
awk '{if ($3 >= $25) print $3; else if ($3 <= $25) print $25}' DELc3vsc4_95.tsv > control_95_DEL_maxEnd.tsv
cut -f1 DELc3vsc4_95.tsv > control_95_DEL_chr.tsv
paste -d _ control_95_DEL_chr.tsv control_95_DEL_minStart.tsv control_95_DEL_maxEnd.tsv > control_95_DEL_identifier.tsv

awk '{if ($2 >= $5) print $5; else if ($5 >= $2) print $2}' DELc3vsc4_IBS.tsv > control_IBS_DEL_minStart.tsv
awk '{if ($3 >= $6) print $3; else if ($3 <= $6) print $6}' DELc3vsc4_IBS.tsv > control_IBS_DEL_maxEnd.tsv
cut -f1 DELc3vsc4_IBS.tsv > control_IBS_DEL_chr.tsv
paste -d _ control_IBS_DEL_chr.tsv control_IBS_DEL_minStart.tsv control_IBS_DEL_maxEnd.tsv > control_IBS_DEL_identifier.tsv

# Generate a reduced summary file of IBS duplications:
paste control_IBS_DEL_identifier.tsv DELc3vsc4_IBS.tsv > DELc3vsc4_IBS_intermediate.tsv
cut -f1,11 DELc3vsc4_IBS_intermediate.tsv > DELc3vsc4_IBS_indexed.tsv
sort DELc3vsc4_IBS_indexed.tsv > DELc3vsc4_IBS_indexed_sorted.tsv

# Use unique identifiers to find DELlications that are 95% similar in size but are non_IBS:
sort control_IBS_DEL_identifier.tsv > control_IBS_DEL_identifier_sorted.tsv
sort control_95_DEL_identifier.tsv > control_95_DEL_identifier_sorted.tsv
grep -F -x -v -f <(grep -o '[^/]*$' control_IBS_DEL_identifier_sorted.tsv) <(grep -o '[^/]*$' control_95_DEL_identifier_sorted.tsv) > control_nonIBS_identifier.tsv

# append non_IBS status to control_nonIBS_identifier.tsv , then sort it:
awk -v OFS='\t' '{print $0,"non_IBS"}' control_nonIBS_identifier.tsv > DELc3vsc4_nonIBS_indexed.tsv
sort DELc3vsc4_nonIBS_indexed.tsv > DELc3vsc4_nonIBS_indexed_sorted.tsv

# Assemble a list of all IBS and non_IBD deletions identified in the control comparison
cat DELc3vsc4_nonIBS_indexed_sorted.tsv DELc3vsc4_IBS_indexed_sorted.tsv > DELc3vsc4_indexed.tsv
sort DELc3vsc4_indexed.tsv > DELc3vsc4_indexed_sorted.tsv

# Add a unique identifer to this list, then sort it
paste control_95_DEL_identifier.tsv DELc3vsc4_95.tsv > DELc3vsc4_95_indexed.tsv
sort DELc3vsc4_95_indexed.tsv > DELc3vsc4_95_indexed_sorted.tsv

# Merge lists to append the final list of all DELlications identified in the control comparison
join -1 1 -2 1 -t $'\t' DELc3vsc4_95_indexed_sorted.tsv DELc3vsc4_indexed_sorted.tsv > control_DEL_appended.tsv

# Remove intermediate files:
rm -rf CNVs_DEL_c3.tsv
rm -rf CNVs_DEL_c4.tsv
rm -rf DELc3vsc4_Str_overlapOnly.tsv
rm -rf SizeDiffDEL_Str.tsv
rm -rf DELc3vsc4_intermediate2_Str.tsv
rm -rf DELc3vsc4_intermediate3_Str.tsv
rm -rf DELc3vsc4_95.tsv
rm -rf DEL_control_comp3_comma.tsv
rm -rf DEL_control_comp4_comma.tsv
rm -rf DEL_control_comp3_chr.tsv
rm -rf DEL_control_comp3_start.tsv
rm -rf DEL_control_comp3_end.tsv
rm -rf DEL_control_comp4_chr.tsv
rm -rf DEL_control_comp4_start.tsv
rm -rf DEL_control_comp4_end.tsv
rm -rf DEL_control_comp3.tsv
rm -rf DEL_control_comp4.tsv
rm -rf DEL_control_comp3_sorted.tsv
rm -rf DEL_control_comp4_sorted.tsv
rm -rf startDiff.tsv
rm -rf endDiff.tsv
rm -rf DELc3vsc4_appended.tsv
rm -rf DELc3vsc4_IBS.tsv
rm -rf control_95_DEL_minStart.tsv
rm -rf control_95_DEL_maxEnd.tsv
rm -rf control_95_DEL_chr.tsv
rm -rf control_95_DEL_identifier.tsv
rm -rf control_IBS_DEL_minStart.tsv
rm -rf control_IBS_DEL_maxEnd.tsv
rm -rf control_IBS_DEL_chr.tsv
rm -rf control_IBS_DEL_identifier.tsv
rm -rf DELc3vsc4_IBS_intermediate.tsv
rm -rf DELc3vsc4_IBS_indexed.tsv
rm -rf DELc3vsc4_IBS_indexed_sorted.tsv
rm -rf control_IBS_DEL_identifier_sorted.tsv
rm -rf control_95_DEL_identifier_sorted.tsv
rm -rf control_nonIBS_identifier.tsv
rm -rf DELc3vsc4_nonIBS_indexed.tsv
rm -rf DELc3vsc4_nonIBS_indexed_sorted.tsv
rm -rf DELc3vsc4_indexed.tsv
rm -rf DELc3vsc4_indexed_sorted.tsv
rm -rf DELc3vsc4_95_indexed.tsv
rm -rf DELc3vsc4_95_indexed_sorted.tsv

#### Duplications

### Read in and sort duplications
awk 'BEGIN{OFS="\t"}{split($1, coords, ","); print coords[1], coords[2],
coords[3], $0 }' ~/step5/data/comparison3/CNV_everted_subset_c3.tsv | sort -k 1,1 -k2,2n > CNVs_DUP_c3.tsv

awk 'BEGIN{OFS="\t"}{split($1, coords, ","); print coords[1], coords[2],
coords[3], $0 }' ~/step5/data/comparison4/CNV_everted_subset_c4.tsv | sort -k 1,1 -k2,2n > CNVs_DUP_c4.tsv

### Implement non-stringent clustering: overlap + 95% size similarity.
bedtools closest -a CNVs_DUP_c3.tsv -b CNVs_DUP_c4.tsv -d | awk 'BEGIN{OFS="\t"} \
{if ($NF < 1) print}' | awk '{if ($21==$43) print $0 }' | awk '!a[$4]++' | awk '!a[$26]++' > DUPc3vsc4_Str_overlapOnly.tsv

# Make the 46th field the ratio of CNV sizes:
awk '{print $12/$32}' DUPc3vsc4_Str_overlapOnly.tsv > SizeDiffDUP_Str.tsv
paste DUPc3vsc4_Str_overlapOnly.tsv SizeDiffDUP_Str.tsv > DUPc3vsc4_intermediate2_Str.tsv

# Implement non-stringent clustering: require absolute overlap and event size similarity of 5%
awk '{if ($46 < 1.05) print}' DUPc3vsc4_intermediate2_Str.tsv > DUPc3vsc4_intermediate3_Str.tsv
awk '{if ($46 > 0.95) print}' DUPc3vsc4_intermediate3_Str.tsv > DUPc3vsc4_95.tsv

# Extract, sort & reassemble coordinate infromation so it can be read by bedtools
cut -f4 DUPc3vsc4_95.tsv > DUP_control_comp3_comma.tsv
cut -f26 DUPc3vsc4_95.tsv > DUP_control_comp4_comma.tsv

cut -d ',' -f1 DUP_control_comp3_comma.tsv > DUP_control_comp3_chr.tsv
cut -d ',' -f2 DUP_control_comp3_comma.tsv > DUP_control_comp3_start.tsv
cut -d ',' -f3 DUP_control_comp3_comma.tsv > DUP_control_comp3_end.tsv

cut -d ',' -f1 DUP_control_comp4_comma.tsv > DUP_control_comp4_chr.tsv
cut -d ',' -f2 DUP_control_comp4_comma.tsv > DUP_control_comp4_start.tsv
cut -d ',' -f3 DUP_control_comp4_comma.tsv > DUP_control_comp4_end.tsv

paste DUP_control_comp3_chr.tsv DUP_control_comp3_start.tsv DUP_control_comp3_end.tsv > DUP_control_comp3.tsv
paste DUP_control_comp4_chr.tsv DUP_control_comp4_start.tsv DUP_control_comp4_end.tsv > DUP_control_comp4.tsv

sort -k 1,1 -k2,2n  DUP_control_comp3.tsv > DUP_control_comp3_sorted.tsv
sort -k 1,1 -k2,2n  DUP_control_comp4.tsv > DUP_control_comp4_sorted.tsv

# match  CNVs and retain only those that overlap
bedtools closest -a DUP_control_comp3_sorted.tsv -b DUP_control_comp4_sorted.tsv -d | awk 'BEGIN{OFS="\t"} \
{if ($NF < 1) print}' > DUPc3vsc4_Str_overlapOnly.tsv

# Calculate start and end differences; append this information
awk '{print ($2-$5)}' DUPc3vsc4_Str_overlapOnly.tsv > startDiff.tsv
awk '{print ($3-$6)}' DUPc3vsc4_Str_overlapOnly.tsv > endDiff.tsv
paste DUPc3vsc4_Str_overlapOnly.tsv startDiff.tsv endDiff.tsv > DUPc3vsc4_appended.tsv

# Append IBS status
awk -v OFS='\t' '{if ($8==0 && $9==0) print $0,"IBS"}' DUPc3vsc4_appended.tsv > DUPc3vsc4_IBS.tsv

# Generate unique identifiers:
awk '{if ($2 >= $24) print $24; else if ($24 >= $2) print $2}'  DUPc3vsc4_95.tsv > control_95_DUP_minStart.tsv
awk '{if ($3 >= $25) print $3; else if ($3 <= $25) print $25}' DUPc3vsc4_95.tsv > control_95_DUP_maxEnd.tsv
cut -f1 DUPc3vsc4_95.tsv > control_95_DUP_chr.tsv
paste -d _ control_95_DUP_chr.tsv control_95_DUP_minStart.tsv control_95_DUP_maxEnd.tsv > control_95_DUP_identifier.tsv

awk '{if ($2 >= $5) print $5; else if ($5 >= $2) print $2}' DUPc3vsc4_IBS.tsv > control_IBS_DUP_minStart.tsv
awk '{if ($3 >= $6) print $3; else if ($3 <= $6) print $6}' DUPc3vsc4_IBS.tsv > control_IBS_DUP_maxEnd.tsv
cut -f1 DUPc3vsc4_IBS.tsv > control_IBS_DUP_chr.tsv
paste -d _ control_IBS_DUP_chr.tsv control_IBS_DUP_minStart.tsv control_IBS_DUP_maxEnd.tsv > control_IBS_DUP_identifier.tsv

# Generate a reduced summary file of IBS duplications:
paste control_IBS_DUP_identifier.tsv DUPc3vsc4_IBS.tsv > DUPc3vsc4_IBS_intermediate.tsv
cut -f1,11 DUPc3vsc4_IBS_intermediate.tsv > DUPc3vsc4_IBS_indexed.tsv
sort DUPc3vsc4_IBS_indexed.tsv > DUPc3vsc4_IBS_indexed_sorted.tsv

# Use unique identifiers to find DUPlications that are 95% similar in size but are non_IBS:
sort control_IBS_DUP_identifier.tsv > control_IBS_DUP_identifier_sorted.tsv
sort control_95_DUP_identifier.tsv > control_95_DUP_identifier_sorted.tsv
grep -F -x -v -f <(grep -o '[^/]*$' control_IBS_DUP_identifier_sorted.tsv) <(grep -o '[^/]*$' control_95_DUP_identifier_sorted.tsv) > control_nonIBS_identifier.tsv

# append non_IBS status to control_nonIBS_identifier.tsv , then sort it:
awk -v OFS='\t' '{print $0,"non_IBS"}' control_nonIBS_identifier.tsv > DUPc3vsc4_nonIBS_indexed.tsv
sort DUPc3vsc4_nonIBS_indexed.tsv > DUPc3vsc4_nonIBS_indexed_sorted.tsv

# Assemble a list of all IBS and non_IBD duplications identified in the control comparison
cat DUPc3vsc4_nonIBS_indexed_sorted.tsv DUPc3vsc4_IBS_indexed_sorted.tsv > DUPc3vsc4_indexed.tsv
sort DUPc3vsc4_indexed.tsv > DUPc3vsc4_indexed_sorted.tsv

# Add a unique identifer to this list, then sort it
paste control_95_DUP_identifier.tsv DUPc3vsc4_95.tsv > DUPc3vsc4_95_indexed.tsv
sort DUPc3vsc4_95_indexed.tsv > DUPc3vsc4_95_indexed_sorted.tsv

# Merge lists to append the final list of all DUPlications identified in the control comparison
join -1 1 -2 1 -t $'\t' DUPc3vsc4_95_indexed_sorted.tsv DUPc3vsc4_indexed_sorted.tsv > control_DUP_appended.tsv

# Remove intermediate files:
rm -rf CNVs_DUP_c3.tsv
rm -rf CNVs_DUP_c4.tsv
rm -rf DUPc3vsc4_Str_overlapOnly.tsv
rm -rf SizeDiffDUP_Str.tsv
rm -rf DUPc3vsc4_intermediate2_Str.tsv
rm -rf DUPc3vsc4_intermediate3_Str.tsv
rm -rf DUPc3vsc4_95.tsv
rm -rf DUP_control_comp3_comma.tsv
rm -rf DUP_control_comp4_comma.tsv
rm -rf DUP_control_comp3_chr.tsv
rm -rf DUP_control_comp3_start.tsv
rm -rf DUP_control_comp3_end.tsv
rm -rf DUP_control_comp4_chr.tsv
rm -rf DUP_control_comp4_start.tsv
rm -rf DUP_control_comp4_end.tsv
rm -rf DUP_control_comp3.tsv
rm -rf DUP_control_comp4.tsv
rm -rf DUP_control_comp3_sorted.tsv
rm -rf DUP_control_comp4_sorted.tsv
rm -rf startDiff.tsv
rm -rf endDiff.tsv
rm -rf DUPc3vsc4_appended.tsv
rm -rf DUPc3vsc4_IBS.tsv
rm -rf control_95_DUP_minStart.tsv
rm -rf control_95_DUP_maxEnd.tsv
rm -rf control_95_DUP_chr.tsv
rm -rf control_95_DUP_identifier.tsv
rm -rf control_IBS_DUP_minStart.tsv
rm -rf control_IBS_DUP_maxEnd.tsv
rm -rf control_IBS_DUP_chr.tsv
rm -rf control_IBS_DUP_identifier.tsv
rm -rf DUPc3vsc4_IBS_intermediate.tsv
rm -rf DUPc3vsc4_IBS_indexed.tsv
rm -rf DUPc3vsc4_IBS_indexed_sorted.tsv
rm -rf control_IBS_DUP_identifier_sorted.tsv
rm -rf control_95_DUP_identifier_sorted.tsv
rm -rf control_nonIBS_identifier.tsv
rm -rf DUPc3vsc4_nonIBS_indexed.tsv
rm -rf DUPc3vsc4_nonIBS_indexed_sorted.tsv
rm -rf DUPc3vsc4_indexed.tsv
rm -rf DUPc3vsc4_indexed_sorted.tsv
rm -rf DUPc3vsc4_95_indexed.tsv
rm -rf DUPc3vsc4_95_indexed_sorted.tsv

###################### Removal of False Positives ######################

# At the comppletion of the Focal Test and the Control Test, we now subtract
#  the CNVs identified in the Control Test from those identified in the
#  the Focal Test. Specifically we test whether CNVs overlap in these two
#  tests.

### Removing false positive deletions:

## Use the min_start and max_end data to test for overlap with Bedtools
# First, decompose the unique identifier into fields that can be read by Bedtools:

cd ~/step6/data/final

cut -f1 ~/step6/data/control/control_DEL_appended.tsv > control_DEL_identifier.tsv
cut -f1 ~/step6/data/focal/focal_DEL_appended.tsv > focal_DEL_identifier.tsv

# Split the identifier into tab-separated fields:
tr '_' '\t' < control_DEL_identifier.tsv 1<> control_DEL_identifier.tsv
tr '_' '\t' < focal_DEL_identifier.tsv 1<> focal_DEL_identifier.tsv

# Ensure these files are sorted:
sort -k 1,1 -k2,2n control_DEL_identifier.tsv > control_DEL_sorted.bed
sort -k 1,1 -k2,2n focal_DEL_identifier.tsv > focal_DEL_sorted.bed


# bedtools intersect -v :Only report those entries in A that have no overlap in B
bedtools intersect -v -a focal_DEL_sorted.bed -b control_DEL_sorted.bed > DEL_sorted.bed

# Generate the list of false positives using intersect -u:
bedtools intersect -a focal_DEL_sorted.bed -b control_DEL_sorted.bed -u > DEL_falsePositives.bed

# Convert back to the unique identifier naming convention and merge with orignal file:
tr '\t' '_' < DEL_sorted.bed 1<> DEL_sorted.tsv
tr '\t' '_' < DEL_falsePositives.bed 1<> DEL_falsePositives.tsv

# re-sort numerically:
sort DEL_sorted.tsv > DEL_sorted_numeric.tsv
sort DEL_falsePositives.tsv > DEL_falsePositives_sorted.tsv

# Append these sorted files with TRUE_POSITIVE / FALSE_POSITIVE status:

awk -v OFS='\t' '{print $0,"TRUE_POSITIVE"}' DEL_sorted_numeric.tsv > DEL_sorted_appended.tsv
awk -v OFS='\t' '{print $0,"FALSE_POSITIVE"}' DEL_falsePositives_sorted.tsv > DEL_falsePositives_sorted_appended.tsv

# Combine these and re-sort:

cat DEL_sorted_appended.tsv DEL_falsePositives_sorted_appended.tsv > DEL_appended.tsv
sort DEL_appended.tsv > DEL_appended_sorted.tsv

# Read in the Focal Test output and sort:
sort ~/step6/data/focal/focal_DEL_appended.tsv > focal_DEL_appended_sorted.tsv

# Merge to append the Focal Test data with the TRUE_POSITIVE/FALSE_POSITIVE status:
join -1 1 -2 1 -t $'\t' focal_DEL_appended_sorted.tsv DEL_appended_sorted.tsv > all_DEL_appended.tsv

# The 49th row specifies whether the CNV is a true positive or not.
# So the final set of deletions can be subest as follows

awk -v OFS='\t' '{if ($49=="TRUE_POSITIVE") print $0}' all_DEL_appended.tsv > final_DEL.tsv

# In our case, no duplications overlapped between tests but, but the above script
# could simply be applied to duplications.

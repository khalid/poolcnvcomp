function step2d() {

distFile=$1 # Output from step1 deletion test
insertSizeDiffCutoff=$2 # The maximum difference in insert size for two read pairs that appear to support the same deletion
minNumSupportingInserts=$3  # Specifies the minimum number of distant read pairs required to retain a cluster
minLenCutoff=$4  # Specifies the minimum length of deletions; 50bp
resFile=$5 # Result file in poolDiffCNV format
summary=$6 # Summary file with the distribution of clusters size, in the directory in which this script is run

awk -v maxDiffIsize=$insertSizeDiffCutoff -v resF=$resFile -v sumaryF=$summary -v numDistInsertCutoff=$minNumSupportingInserts -v delLenCutoff=$minLenCutoff '
BEGIN {nbRS=0; nbIS=0; OFS="\t"}
{

    if (NR==1)
    {
        cl = 1;
        ls = $3;
        le = $4; max_le = le
        rs = $6; min_rs = rs
        dprime = rs - le +1
        clsize = 1
        clreads = $3","$4","$6","$7","$12","$1
        chr = $2
    }
    else
    {
        Curr_dprime = $6 - $4 + 1
        if ($4 <= (rs - delLenCutoff) && (Curr_dprime <= dprime + maxDiffIsize))
        {
            reason=""
            clsize += 1
            if ($6 < min_rs) min_rs = $6
            if ($4 > max_le) max_le = $4
            clreads = $3","$4","$6","$7","$12","$1"\t"clreads
            Lstarts[clsize] = $3
        }
        else #New cluster
        {
            if ( clsize >= numDistInsertCutoff && (min_rs - max_le) >= delLenCutoff )
            {
                print chr","max_le","min_rs, clsize, clreads > resF
                cls[clsize]++
                retained_cl += 1
                # here we will test if the cluster encompass more than one deletion
                # rule : one of the inserts left read start at a position >= min_rs
                outliers = 0
                for (ins=1; ins <= clsize; ins++)
                {
                    if (Lstarts[ins] >= min_rs) outliers++
                }

                if (outliers > 0) {print "False positive!",chr, max_le","min_rs, clsize,outliers , clreads }
            }
            else
            {
                #print chr, max_le","min_rs, clsize,min_rs - max_le
            }

            if ($4 > (rs- delLenCutoff)) {reason="RS"; nbRS++} else {reason="IS"; nbIS++}
            cl++
            ls = $3;
            le = $4;  max_le = le
            rs = $6;  min_rs = rs
            dprime = rs - le +1
            clsize = 1
            clreads = $3","$4","$6","$7","$12","$1
        }
    }

    #print $0, cl, clsize , reason

}
END{
    #last cluster
    if ( clsize >= numDistInsertCutoff && (min_rs - max_le) >= delLenCutoff )
    {
     print chr","max_le","min_rs, clsize, clreads > resF
     cls[clsize]++
     retained_cl +=1
     # here we will test if the cluster encompass more than one deletion
     # rule : one of the inserts left read start at a position >= min_rs
            outliers = 0
            for (ins=1; ins <= clsize; ins++)
            {
               if (Lstarts[ins] >= min_rs) outliers++
            }

            if (outliers > 0) {print chr, max_le","min_rs, clsize,outliers , clreads }
    }

    #summary
    print "#read_pairs", "#clusters", "retained_cluster", "#RsRule","#ISRule"
    print NR, cl, retained_cl, nbRS,   nbIS

    print "\nclSize\tcount"  > sumaryF
    for (s in cls) print s, cls[s]  > sumaryF
}
' $distFile
}

# Iterate over pools and chromosomes

for chr in `seq 1 19` X
do
  cd Chr$chr

for pool in 1
  do
     step2d ~/step1/data/Chr${chr}/c${chr}p${pool}d.txt 232 5 50 ~/step2/data/Chr${chr}/c${chr}p${pool}d.tsv ~/step2/data/c${chr}p${pool}_summary_d.tsv
  done
for pool in 2
  do
    step2d ~/step1/data/Chr${chr}/c${chr}p${pool}d.txt 215 5 50 ~/step2/data/Chr${chr}/c${chr}p${pool}d.tsv ~/step2/data/c${chr}p${pool}_summary_d.tsv
  done
for pool in 3
  do
    step2d ~/step1/data/Chr${chr}/c${chr}p${pool}d.txt 247 5 50 ~/step2/data/Chr${chr}/c${chr}p${pool}d.tsv ~/step2/data/c${chr}p${pool}_summary_d.tsv
  done
for pool in 4
  do
    step2d ~/step1/data/Chr${chr}/c${chr}p${pool}d.txt 242 5 50 ~/step2/data/Chr${chr}/c${chr}p${pool}d.tsv ~/step2/data/c${chr}p${pool}_summary_d.tsv
  done
for pool in 5
  do
    step2d ~/step1/data/Chr${chr}/c${chr}p${pool}d.txt 216 5 50 ~/step2/data/Chr${chr}/c${chr}p${pool}d.tsv ~/step2/data/c${chr}p${pool}_summary_d.tsv
  done
for pool in 6
  do
    step2d ~/step1/data/Chr${chr}/c${chr}p${pool}d.txt 246 5 50 ~/step2/data/Chr${chr}/c${chr}p${pool}d.tsv ~/step2/data/c${chr}p${pool}_summary_d.tsv
  done
for pool in 7
  do
    step2d ~/step1/data/Chr${chr}/c${chr}p${pool}d.txt 149 5 50 ~/step2/data/Chr${chr}/c${chr}p${pool}d.tsv ~/step2/data/c${chr}p${pool}_summary_d.tsv
  done

  cd ..
done

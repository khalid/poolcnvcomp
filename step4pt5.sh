function step4pt5(){

    chr=$1
    comparison=$2

    # Define the pools relevant to the specified comparison

    if [[ ${comparison} -eq 1 ]]; then
        poolA=2
        poolB=3
        altname=1
    elif [[ ${comparison} -eq 2 ]]; then
        poolA=4
        poolB=7
        altname=2
    elif [[ ${comparison} -eq 3 ]]; then
        poolA=2
        poolB=4
        altname=10
    elif [[ ${comparison} -eq 4 ]]; then
        poolA=3
        poolB=7
        altname=11
    elif [[ ${comparison} -eq 5 ]]; then
        poolA=4
        poolB=6
    elif [[ ${comparison} -eq 6 ]]; then
        poolA=2
        poolB=5
    elif [[ ${comparison} -eq 7 ]]; then
        poolA=5
        poolB=6
    elif [[ ${comparison} -eq 8 ]]; then
        poolA=1
        poolB=5
    elif [[ ${comparison} -eq 9 ]]; then
        poolA=1
        poolB=6
    elif [[ ${comparison} -eq 10 ]]; then
    	poolA=2
    	poolB=4
    elif [[ ${comparison} -eq 11 ]]; then
    	poolA=3
    	poolB=7
    fi

    ### EVERTED READS (DUPLICATIONS)

    ## Assign size window categories to each CNV

    cut -f7 ~/step4/data/comparison${altname}/c${chr}_P${poolA}vsP${poolB}d_comp${altname}_distant.tsv | awk -F $'\t' 'BEGIN {OFS = FS} \
     {if ($1 < 75) print "50"; \
     else if ($1 >= 75 && $1 < 125) print "100"; \
     else if ($1 >= 125 && $1 < 175) print "150"; \
     else if ($1 >= 175 && $1 < 350) print "200"; \
     else if ($1 >= 350 && $1 < 750) print "500"; \
     else if ($1 >= 750 && $1 < 1500) print "1000"; \
     else if ($1 >= 1500 && $1 < 3500) print "2000"; \
     else if ($1 >= 3500 && $1 < 6500) print "5000";\
     else if ($1 >= 6500) print "8000"}' > windowSizes_Chr${chr}_Pools${poolA}vs${poolB}_e.tsv

    ## Append the 10th field with window category information
    paste ~/step4/data/comparison${altname}/c${chr}_P${poolA}vsP${poolB}d_comp${altname}_distant.tsv \
    windowSizes_Chr${chr}_Pools${poolA}vs${poolB}_e.tsv > S4_Window_everted_Chr${chr}_Pools${poolA}vs${poolB}.tsv

    ## Append the 11th field with the difference in number of supporting inserts (DiffSuppIns):

    awk -F $'\t' 'BEGIN {OFS = FS} {print $3-$5 }' S4_Window_everted_Chr${chr}_Pools${poolA}vs${poolB}.tsv > DSI_e.tsv
    paste S4_Window_everted_Chr${chr}_Pools${poolA}vs${poolB}.tsv DSI_e.tsv > S4_DSI_everted_Chr${chr}_Pools${poolA}vs${poolB}.tsv

     ## Calculate the read depth ratio and append the 12th field with this value

    # calculate the per-nucleotide read depth in pool A if the CNV was present in A
    awk -F $'\t' 'BEGIN {OFS = FS}{Wsize=$7; depth=$6; print (depth/Wsize)*100}' S4_DSI_everted_Chr${chr}_Pools${poolA}vs${poolB}.tsv  > PerNtDepthA_e.tsv
    # calculate the per-nucleotide read depth in pool B if the CNV was present in B
    awk -F $'\t' 'BEGIN {OFS = FS}{Wsize=$7; depth=$8; print (depth/Wsize)*100}' S4_DSI_everted_Chr${chr}_Pools${poolA}vs${poolB}.tsv  > PerNtDepthB_e.tsv
    paste PerNtDepthA_e.tsv PerNtDepthB_e.tsv > ObsDepth_e.tsv
    awk -F $'\t' 'BEGIN {OFS = FS}{if ($2 != 0) print $1/$2; else if ($2 == 0) print "inf" }' ObsDepth_e.tsv > ObsRDR_e.tsv
    paste S4_DSI_everted_Chr${chr}_Pools${poolA}vs${poolB}.tsv ObsRDR_e.tsv > S4_ObsRDR_everted_Chr${chr}_Pools${poolA}vs${poolB}.tsv

    ## Assign a status (qualitative difference or quantitative difference) to each CNV and apppend the 13th field with this status
    awk -F $'\t' 'BEGIN {OFS = FS}{if ($2 != "NA" && $4 != "NA") print "Quantitative"; else if ($2 = "NA" || $4 = "NA") print "Qualitative"}' \
    S4_ObsRDR_everted_Chr${chr}_Pools${poolA}vs${poolB}.tsv > QQ_e.tsv
    paste S4_ObsRDR_everted_Chr${chr}_Pools${poolA}vs${poolB}.tsv QQ_e.tsv > S4_QQ_everted_Chr${chr}_Pools${poolA}vs${poolB}.tsv
    # Qualitative/Quantitative status is now the 13th field



    paste S4_QQ_everted_Chr${chr}_Pools${poolA}vs${poolB}.tsv MNSI_everted.tsv > S4_MNSI_everted_Chr${chr}_Pools${poolA}vs${poolB}.tsv

    ## Read in the upper 95% read depth ratio threshold and assign this value to the 15th field

    infile="S4_MNSI_everted_Chr"${chr}"_Pools"${poolA}"vs"${poolB}".tsv"
    outfile="S4_MNSI_everted_Chr"${chr}"_Pools"${poolA}"vs"${poolB}"_Upper.tsv"
    thresholdsfile="~/step4pt5/data/RDR_95/C"${comparison}"/Up95_Chr"${chr}"_C"${comparison}".tsv"


    awk -v outf=$outfile -F $'\t' 'BEGIN {OFS = FS}  { if (NR == FNR ) {d[$1]=$2}
    else {
    print $0, d[$10] > outf
    }}
    ' $thresholdsfile $infile

    # 16th field is lower threshold

    infile="S4_MNSI_everted_Chr"${chr}"_Pools"${poolA}"vs"${poolB}"_Upper.tsv"
    outfile="S4_MNSI_everted_Chr"${chr}"_Pools"${poolA}"vs"${poolB}"_Lower.tsv"
    thresholdsfile="~/step4pt5/data/RDR_95/C"${comparison}"/Lo95_Chr"${chr}"_C"${comparison}".tsv"

    awk -v outf=$outfile -F $'\t' 'BEGIN {OFS = FS} { if (NR == FNR ) {d[$1]=$2}
    else {
    print $0, d[$10] > outf
    }}
    ' $thresholdsfile $infile

    # 17th field labels Everted reads

    awk -F $'\t' 'BEGIN {OFS = FS} {if ($14 == "T" || $14 == "F") print "everted"}' S4_MNSI_everted_Chr${chr}_Pools${poolA}vs${poolB}_Lower.tsv > EVERTED.tsv
    paste S4_MNSI_everted_Chr${chr}_Pools${poolA}vs${poolB}_Lower.tsv EVERTED.tsv > S4_labeled_everted_Chr${chr}_Pools${poolA}vs${poolB}.tsv


    # 18th field labels legitimate tandem duplications that belong to a specific pool (DUP, for everted reads), legitimate deletions (DEL, for distant reads) and false positives (FP)

    # 18th field labels legitimate tandem duplications (DUP) in to a specific pool (A or B) and false positives (FP).
    # FPs are generated when the DiffSuppIns value is not in the same 'direction' as the read depth ratio.
    # If there is a duplication in pool A, there should be more everted read
    # pairs in A compared to B and a higher read depth in A compared to B (and vice versa for b).

    awk -F $'\t' 'BEGIN {OFS = FS} {if ($11 > 0 && $12 > 1) print "DUP_A"; \
    else if ($11 < 0 && $12 < 1) print "DUP_B"; \
    else print "FP"}' S4_labeled_everted_Chr${chr}_Pools${poolA}vs${poolB}.tsv > S4_CNVAB_e.txt

    paste S4_labeled_everted_Chr${chr}_Pools${poolA}vs${poolB}.tsv S4_CNVAB_e.txt > S4_CNVAB_everted_Chr${chr}_Pools${poolA}vs${poolB}.tsv

    # 19th field labels legitimate tandem duplications in general

    awk -F $'\t' 'BEGIN {OFS = FS} {if ($18 == "DUP_A" || $18 == "DUP_B") print "DUP"; else print "FP"}' S4_CNVAB_everted_Chr${chr}_Pools${poolA}vs${poolB}.tsv > DUP_FP.tsv
    paste S4_CNVAB_everted_Chr${chr}_Pools${poolA}vs${poolB}.tsv DUP_FP.tsv > S4_appended_everted_Chr${chr}_Pools${poolA}vs${poolB}.tsv

    # remove intermediate files

    rm -rf windowSizes_Chr${chr}_Pools${poolA}vs${poolB}_e.tsv
    rm -rf S4_Window_everted_Chr${chr}_Pools${poolA}vs${poolB}.tsv
    rm -rf DSI_e.tsv
    rm -rf S4_DSI_everted_Chr${chr}_Pools${poolA}vs${poolB}.tsv
    rm -rf PerNtDepthA_e.tsv
    rm -rf PerNtDepthB_e.tsv
    rm -rf ObsDepth_e.tsv
    rm -rf ObsRDR_e.tsv
    rm -rf EVERTED.tsv
    rm -rf S4_ObsRDR_everted_Chr${chr}_Pools${poolA}vs${poolB}.tsv
    rm -rf UpThreshold99.tsv
    rm -rf UpThreshold99.tsv
    rm -rf MNSI_everted.tsv
    rm -rf QQ_e.tsv
    rm -rf S4_QQ_everted_Chr${chr}_Pools${poolA}vs${poolB}.tsv
    rm -rf S4_MNSI_everted_Chr${chr}_Pools${poolA}vs${poolB}.tsv
    rm -rf S4_MNSI_everted_Chr${chr}_Pools${poolA}vs${poolB}_Upper.tsv
    rm -rf S4_MNSI_everted_Chr${chr}_Pools${poolA}vs${poolB}_Lower.tsv
    rm -rf S4_labeled_everted_Chr${chr}_Pools${poolA}vs${poolB}.tsv
    rm -rf S4_CNVAB_e.txt
    rm -rf S4_CNVAB_everted_Chr${chr}_Pools${poolA}vs${poolB}.tsv
    rm -rf DUP_FP.tsv

    ### DISTANT READS (DELETIONS)

    ## Assign size window categories to each CNV
    cut -f7 ~/step4/data/comparison${altname}/c${chr}_P${poolA}vsP${poolB}d_comp${altname}_distant.tsv | awk -F $'\t' 'BEGIN {OFS = FS} { \
      if ($1 < 75) print "50"; \
      else if ($1 >= 75 && $1 < 125) print "100"; \
      else if ($1 >= 125 && $1 < 175) print "150"; \
      else if ($1 >= 175 && $1 < 350) print "200"; \
      else if ($1 >= 350 && $1 < 750) print "500"; \
      else if ($1 >= 750 && $1 < 1500) print "1000"; \
      else if ($1 >= 1500 && $1 < 3500) print "2000"; \
      else if ($1 >= 3500 && $1 < 6500) print "5000"; \
      else if ($1 >= 6500) print "8000"}' > windowSizes_Chr${chr}_Pools${poolA}vs${poolB}.tsv

    ## Append the 10th field with window category information
    paste ~/step4/data/comparison${altname}/c${chr}_P${poolA}vsP${poolB}d_comp${altname}_distant.tsv \
    windowSizes_Chr${chr}_Pools${poolA}vs${poolB}.tsv > S4_Window_distant_Chr${chr}_Pools${poolA}vs${poolB}.tsv

     ## Append the 11th field with the difference in number of supporting inserts (DiffSuppIns):
    awk -F $'\t' 'BEGIN {OFS = FS}{print $3-$5 }' S4_Window_distant_Chr${chr}_Pools${poolA}vs${poolB}.tsv > DSI.tsv
    paste S4_Window_distant_Chr${chr}_Pools${poolA}vs${poolB}.tsv DSI.tsv > S4_DSI_distant_Chr${chr}_Pools${poolA}vs${poolB}.tsv

    ## Calculate the read depth ratio and append the 12th field with this value

    # calculate the per-nucleotide read depth in pool A if the CNV was present in A
    awk -F $'\t' 'BEGIN {OFS = FS}{Wsize=$7; depth=$6; print (depth/Wsize)*100}' S4_DSI_distant_Chr${chr}_Pools${poolA}vs${poolB}.tsv  > PerNtDepthA.tsv
    # calculate the per-nucleotide read depth in pool B if the CNV was present in B
    awk -F $'\t' 'BEGIN {OFS = FS}{Wsize=$7; depth=$8; print (depth/Wsize)*100}' S4_DSI_distant_Chr${chr}_Pools${poolA}vs${poolB}.tsv  > PerNtDepthB.tsv
    paste PerNtDepthA.tsv PerNtDepthB.tsv > ObsDepth.tsv
    awk -F $'\t' 'BEGIN {OFS = FS}{if ($2 != 0) print $1/$2; else if ($2 == 0) print "inf" }' ObsDepth.tsv > ObsRDR.tsv
    paste S4_DSI_distant_Chr${chr}_Pools${poolA}vs${poolB}.tsv ObsRDR.tsv > S4_ObsRDR_distant_Chr${chr}_Pools${poolA}vs${poolB}.tsv

    # Assign a status (qualitative difference or quantitative difference) to each CNV and apppend the 13th field with this status
    awk -F $'\t' 'BEGIN {OFS = FS} {if ($2 != "NA" && $4 != "NA") print "Quantitative"; else if ($2 = "NA" || $4 = "NA") print "Qualitative"}' \
    S4_ObsRDR_distant_Chr${chr}_Pools${poolA}vs${poolB}.tsv > QQ.tsv
    paste S4_ObsRDR_distant_Chr${chr}_Pools${poolA}vs${poolB}.tsv QQ.tsv > S4_QQ_distant_Chr${chr}_Pools${poolA}vs${poolB}.tsv


    # For clusters of distant reads, identify those which have at least 5 supporting reads  (MNSI>5) and append the 14th field with this status
    awk -F $'\t' 'BEGIN {OFS = FS} {if ($2 != "NA" && $4 != "NA" && ($3 <= 5 || $5 <= 5)) print "F"; \
    else if ($2 != "NA" && $4 != "NA" && ($3 >= 5 || $5 >= 5)) print "T"; \
    else if ($2 != "NA" && $3 <= 5) print "F";\
    else if ($2 != "NA" && $3 >= 5) print "T"; \
    else if ($4 != "NA" && $5 <= 5) print "F";\
    else if ($4 != "NA" && $5 >= 5) print "T"}' S4_QQ_distant_Chr${chr}_Pools${poolA}vs${poolB}.tsv > MNSI_distant.tsv
    paste S4_QQ_distant_Chr${chr}_Pools${poolA}vs${poolB}.tsv MNSI_distant.tsv > S4_MNSI_distant_Chr${chr}_Pools${poolA}vs${poolB}.tsv

    # Read in the upper threshold  and assign this value to the 15th field

    infile="S4_MNSI_distant_Chr"${chr}"_Pools"${poolA}"vs"${poolB}".tsv"
    outfile="S4_MNSI_distant_Chr"${chr}"_Pools"${poolA}"vs"${poolB}"_Upper.tsv"
    thresholdsfile="~/step4pt5/data/RDR_95/C"${comparison}"/Up95_Chr"${chr}"_C"${comparison}".tsv"

    awk -v outf=$outfile -F $'\t' 'BEGIN {OFS = FS}  { if (NR == FNR ) {d[$1]=$2}
    else {
    print $0, d[$10] > outf
    }}
    ' $thresholdsfile $infile

    # 16th field is lower threshold

    infile="S4_MNSI_distant_Chr"${chr}"_Pools"${poolA}"vs"${poolB}"_Upper.tsv"
    outfile="S4_MNSI_distant_Chr"${chr}"_Pools"${poolA}"vs"${poolB}"_Lower.tsv"
    thresholdsfile="~/step4pt5/data/RDR_95/C"${comparison}"/Lo95_Chr"${chr}"_C"${comparison}".tsv"

    awk -v outf=$outfile -F $'\t' 'BEGIN {OFS = FS}  { if (NR == FNR ) {d[$1]=$2}
    else {
    print $0, d[$10] > outf
    }}
    ' $thresholdsfile $infile

    # 17th field labels Distant reads

    awk -F $'\t' 'BEGIN {OFS = FS} {if ($14 == "T" || $14 == "F") print "distant"}' S4_MNSI_distant_Chr${chr}_Pools${poolA}vs${poolB}_Lower.tsv > distant.tsv
    paste S4_MNSI_distant_Chr${chr}_Pools${poolA}vs${poolB}_Lower.tsv distant.tsv > S4_labeled_distant_Chr${chr}_Pools${poolA}vs${poolB}.tsv


    # 18th field labels legitimate tandem deletions (DEL) in to a specific pool (A or B) and false positives (FP).
    # FPs are generated when the DiffSuppIns value is not in the same 'direction' as the read depth ratio.
    # If there is a deletion in pool A, there should be more distant read
    # pairs in A compared to B and a lower read depth in A compared to B (and vice versa for b).

    awk -F $'\t' 'BEGIN {OFS = FS} {if ($11 > 0 && $12 < 1) print "DEL_A"; \
    else if ($11 < 0 && $12 > 1) print "DEL_B"; \
    else print "FP"}' S4_labeled_distant_Chr${chr}_Pools${poolA}vs${poolB}.tsv > CNVAB2.tsv

    paste S4_labeled_distant_Chr${chr}_Pools${poolA}vs${poolB}.tsv CNVAB2.tsv > S4_CNVAB_distant_Chr${chr}_Pools${poolA}vs${poolB}.tsv

    # 19th field labels each as DEL (legitimate deletion) vs FP (false positive)

    awk -F $'\t' 'BEGIN {OFS = FS} {if ($18 == "DEL_A" || $18 == "DEL_B") print "DEL"; else print "FP"}' S4_CNVAB_distant_Chr${chr}_Pools${poolA}vs${poolB}.tsv > DEL_FP.tsv
    paste S4_CNVAB_distant_Chr${chr}_Pools${poolA}vs${poolB}.tsv DEL_FP.tsv > S4_appended_distant_Chr${chr}_comparison${comparison}.tsv

        # All intermediate files are removed
    rm -rf windowSizes_Chr${chr}_Pools${poolA}vs${poolB}.tsv
    rm -rf S4_Window_distant_Chr${chr}_Pools${poolA}vs${poolB}.tsv
    rm -rf DSI.tsv
    rm -rf S4_DSI_distant_Chr${chr}_Pools${poolA}vs${poolB}.tsv
    rm -rf PerNtDepthA.tsv
    rm -rf PerNtDepthB.tsv
    rm -rf ObsDepth.tsv
    rm -rf ObsRDR.tsv
    rm -rf S4_ObsRDR_distant_Chr${chr}_Pools${poolA}vs${poolB}.tsv
    rm -rf UpThreshold99.tsv
    rm -rf MNSI_distant.tsv
    rm -rf distant.tsv
    rm -rf QQ.tsv
    rm -rf S4_QQ_distant_Chr${chr}_Pools${poolA}vs${poolB}.tsv
    rm -rf S4_MNSI_distant_Chr${chr}_Pools${poolA}vs${poolB}.tsv
    rm -rf S4_MNSI_distant_Chr${chr}_Pools${poolA}vs${poolB}_Upper.tsv
    rm -rf S4_MNSI_distant_Chr${chr}_Pools${poolA}vs${poolB}_Lower.tsv
    rm -rf S4_labeled_distant_Chr${chr}_Pools${poolA}vs${poolB}.tsv
    rm -rf S4_CNVAB_distant_Chr${chr}_Pools${poolA}vs${poolB}.tsv
    rm -rf DEL_FP.tsv
    rm -rf CNVAB2.tsv


}

## focal comparisons
# comparison 1
cd ~/step4pt5/data/comparison1
for chr in `seq 1 19` X
do
 step4pt5 $chr 1
done
cat S4_appended_distant_Chr*.tsv > S4_distant_c1.tsv # collate all DEL output
cat S4_appended_everted_Chr*.tsv > S4_everted_c1.tsv # collate all DUP output
cd ~

# comparison 2
cd ~/step4pt5/data/comparison2
for chr in `seq 1 19` X
do
 step4pt5 $chr 2
done
cat S4_appended_distant_Chr*.tsv > S4_distant_c2.tsv # collate all DEL output
cat S4_appended_everted_Chr*.tsv > S4_everted_c2.tsv # collate all DUP output
cd ~

# comparison 3 (referred to as comparison 10 in step 4)
cd ~/step4pt5/data/comparison3
for chr in `seq 1 19` X
do
 step4pt5 $chr 3
done
cat S4_appended_distant_Chr*.tsv > S4_distant_c3.tsv # collate all DEL output
cat S4_appended_everted_Chr*.tsv > S4_everted_c3.tsv # collate all DUP output
cd ~

# comparison 4 (referred to as comparison 11 in step 4)
cd ~/step4pt5/data/comparison4
for chr in `seq 1 19` X
do
 step4pt5 $chr 4
done
cat S4_appended_distant_Chr*.tsv > S4_distant_c4.tsv # collate all DEL output
cat S4_appended_everted_Chr*.tsv > S4_everted_c4.tsv # collate all DUP output
cd ~

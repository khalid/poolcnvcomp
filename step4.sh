#Implement step4 (d and e) for comparisons 10 and 11 (the two control comparisons)

function step4() {
chr=$1
comparison=$2

# Read in the masked-regions file for the relevant chromosome
# Masked regions files available at https:/genome.ucsc.edu/cgi-bin/hgTables
MaskedRegionsFile=~/s4/data/maskFiles/maskedRegions_GRCm38_Chr${chr}.bed

# Define comparisons
if [[ ${comparison} -eq 1 ]]; then # Focal comparison 1
  poolA=2
  poolB=3
elif [[ ${comparison} -eq 2 ]]; then # Focal comparison 2
  poolA=4
  poolB=7
elif [[ ${comparison} -eq 3 ]]; then # Control comparison 1
  poolA=2
  poolB=4
elif [[ ${comparison} -eq 4 ]]; then # Control comparison 2
  poolA=3
  poolB=7
elif [[ ${comparison} -eq 5 ]]; then
  poolA=4
  poolB=6
elif [[ ${comparison} -eq 6 ]]; then
  poolA=2
  poolB=5
elif [[ ${comparison} -eq 7 ]]; then
  poolA=5
  poolB=6
elif [[ ${comparison} -eq 8 ]]; then
  poolA=1
  poolB=5
elif [[ ${comparison} -eq 9 ]]; then
  poolA=1
  poolB=6
elif [[ ${comparison} -eq 10 ]]; then # also control comparison 1
  poolA=2
  poolB=4
elif [[ ${comparison} -eq 11 ]]; then # also control comparison 2
  poolA=3
  poolB=7
fi

# Read in the relevant .bam file
# Our sequence data is available on Shot Read Archive, bioproject PRJNA603262
ficBamA=filtered_Test_All_reads_GRCm38_Pool${poolA}.sorted.reheaded.ddup.bam.REF_${chr}.bam

ficBamB=filtered_Test_All_reads_GRCm38_Pool${poolB}.sorted.reheaded.ddup.bam.REF_${chr}.bam

# distant read-pair clusters
python step4_countReadPairsInCNV-KH-pySam.py \
~/step3/data/comparison${comparison}/c${chr}P${poolA}vsP${poolB}_distant.tsv \
  $ficBamA $MaskedRegionsFile > ~/step4/data/comparison${comparison}/c${chr}_Aonly_distant.tsv # this is an intermediate file

python step4_countReadPairsInCNV-KH-pySam.py \
~/step4/data/comparison${comparison}/c${chr}_Aonly_distant.tsv \
  $ficBamB $MaskedRegionsFile  > ~/step4/data/comparison${comparison}/c${chr}_P${poolA}vsP${poolB}d_comp${comparison}_distant.tsv


# everted read-pair clusters only
python step4_countReadPairsInCNV-KH-pySam.py \
~/step3/data/comparison${comparison}/c${chr}P${poolA}vsP${poolB}_everted.tsv \
  $ficBamA $MaskedRegionsFile > ~/step4/data/comparison${comparison}/c${chr}_Aonly_everted.tsv # this is an intermediate file

python step4_countReadPairsInCNV-KH-pySam.py \
~/step4/data/comparison${comparison}/c${chr}_Aonly_everted.tsv \
  $ficBamB $MaskedRegionsFile  > ~/step4/data/comparison${comparison}/c${chr}_P${poolA}vsP${poolB}d_comp${comparison}_everted.tsv

# remove the relevant data at the end of this part of the loop
rm -f   $ficBamA
rm -f   $ficBamB

}
# comparison 1 (Our focal comparison 1), for example
for ch in `seq 1 19` X
do
   step4 $ch 1 &
done
